import pydicom as dicom
import pandas as pd
import os 
import glob
from multiprocessing import Pool
#import fnmatch
import pandas as pd
import tqdm
import time
import six
##from args import *
import unicodedata
import re
import traceback


def _remove_accents(filename):
    """
    Function that will try to remove accents from a unicode string to be used in a filename.
    input filename should be either an ascii or unicode string
    """
    # noinspection PyBroadException
    try:
        filename = filename.replace(" ", "_")
        if isinstance(filename, type(six.u(''))):
            unicode_filename = filename
        else:
            unicode_filename = six.u(filename)
        cleaned_filename = unicodedata.normalize('NFKD', unicode_filename).encode('ASCII', 'ignore').decode('ASCII')

        cleaned_filename = re.sub('[^\w\s-]', '', cleaned_filename.strip().lower())
        cleaned_filename = re.sub('[-\s]+', '-', cleaned_filename)

        return cleaned_filename
    except:
        traceback.print_exc()
        return filename

def get_ROIs(scan):

    structs = '' 
    # first create a map so that we can easily trace referenced_roi_number back to its metadata
    metadata_mappings = {}
    for contour_metadata in scan.StructureSetROISequence:
        
        metadata_mappings[contour_metadata.ROINumber] = contour_metadata

    for contour_sequence in scan.ROIContourSequence:
        contour_data = {}
        metadata = metadata_mappings[contour_sequence.ReferencedROINumber] # retrieve metadata
        if hasattr(metadata, 'ROIName'):
            structs = structs+_remove_accents(metadata.ROIName)+','
    
    return structs[:-1]

def save_dcm_info(filename):
    scan = dicom.dcmread(filename)
    #print(filename)
    #n.append(scan.SOPClassUID)
    #if scan.SOPClassUID not in n:
    info = {}

    info['SOPClassUID'] = scan.SOPClassUID
    info['PatientName'] = scan.PatientName
    info['PatientID'] = scan.PatientID
    info['Modality'] = scan.Modality
    try:
        info['StudyDescription'] = scan.StudyDescription
    except:
        info['StudyDescription'] = 'nan'
    try:
        info['StudyDate'] = scan.StudyDate
    except:
        info['StudyDate'] = 'nan'
    try:
        info['SeriesDate'] = scan.SeriesDate
    except:
        info['SeriesDate'] = 'nan'
    try:
        info['AcquisitionDate'] = scan.AcquisitionDate
    except:
        info['AcquisitionDate'] = 'nan'
           #n.append(scan.SOPClassUID)
    info['GTVt'] = 'nan'  
    info['GTVn'] = 'nan'  
    if scan.Modality == 'RTSTRUCT':
        info['ROIs'] = get_ROIs(scan)
        if 'gtv_t' in info['ROIs']:
            info['GTVt'] = 1
        else:
            info['GTVt'] = 0

        if 'gtv_n' in info['ROIs']:
            info['GTVn'] = 1
        else:
            info['GTVn'] = 0

    return info


if __name__ == "__main__":

    #parser = ConvertArgParser()
    #args = parser.parse_args()
    
    #over-write number of threads
    #args.nb_threads = 8

    #input output folds
    #dicom_path
    #in_dir = os.path.normpath(args.in_locs)
    #info_loc = os.path.normpath(args.info_loc)

    in_dir = os.path.normpath('/mnt/faststorage/hndata/data_series/2_renamed/')
    info_loc = os.path.normpath('/mnt/faststorage/hndata/data_series/')

    #folder = "HNSCC"
    pattern = "*.dcm"
    p_num = 8

    filenames = []
    for path, subdirs, files in os.walk(in_dir):
        for name in files:
            if name.endswith('.dcm'):
                filenames.append(os.path.join(path, name))
    print('File names loaded.')

    print('Start reading dicom files with %d processes..' %p_num)
    try:
        pool = Pool(processes=p_num)      
        resultset = list(tqdm.tqdm(pool.imap(save_dcm_info, filenames, chunksize=100), total=len(filenames)))

        df = pd.DataFrame(resultset)
        df = df.drop_duplicates()
    
        df.to_csv(info_loc+'dicom_summary_report.csv')

        print("finished load")
    except:
        print('There was a problem multithreading the Pool')
        raise