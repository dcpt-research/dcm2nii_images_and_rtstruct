import os
import glob

default_num_threads = 32

join = os.path.join
isdir = os.path.isdir


base = os.path.normpath()
input_dir = os.path.normpath()
output_dir = os.path.normpath()

