import pydicom
from pathlib import Path
import SimpleITK as sitk
import argparse
import logging


def construct_series_dict(folder_in):
    """
    Create dictionary which sorts files by series uid
    -------------------------------------------------
    Input:
            folder_in   path of folder containing dicom
                        files to be sorted (type: Path)
    """
    series_dict = {}
    for dicom_file in list(folder_in.rglob('*.dcm')):
        metadata = pydicom.dcmread(str(dicom_file), stop_before_pixels=True)
        series_uid = metadata.SeriesInstanceUID
        series_number = metadata.SeriesNumber
        if series_uid in series_dict:
            series_dict[series_uid]['file'].append(dicom_file)
            series_dict[series_uid]['series_number'].append(series_number)
        else:
            series_dict[series_uid] = {}
            series_dict[series_uid]['file'] = [dicom_file]
            series_dict[series_uid]['series_number'] = [series_number]
    return series_dict


def dicom2nrrd(dcm_files, folder_out):
    """
    Convert dicom series in a folder to nrrd sorted
    by temporal position. Code modified from script
    dicom_to_nrrd.py by Jonas Teuwen.
    --------------------------------------------------
    Input:
            folder_in   path of folder containing dicom files
                        to be sorted (type: Path)
            folder_out  path of folder where the series are
                        stored (type: Path)
    Output:
            <none>
    """

    # Construct output folder
    if not folder_out.is_dir():
        folder_out.mkdir()

    # Initialize and sort dcm files on TemporalPositionIndentifier
    dcm_files = sorted(dcm_files, key=lambda f: pydicom.dcmread(str(f),
                       stop_before_pixels=True).TemporalPositionIdentifier)
    dcm_loaded = [pydicom.dcmread(str(f), stop_before_pixels=True) for f in dcm_files]  # Extremely inefficient!!
    logging.info(f'\t {len(dcm_files)} dcm files were found')

    # Infer series ids: there should be only one
    dcm_series_uid = list(set([f.SeriesInstanceUID for f in dcm_loaded]))
    if len(dcm_series_uid) != 1:
        raise RuntimeError(f'\t multiple dicom series detected!')

    # Count number of distinct temporal positions
    num_temp_pos = dcm_loaded[0].NumberOfTemporalPositions
    temp_pos_uid = [dcm.TemporalPositionIdentifier for dcm in dcm_loaded]
    assert num_temp_pos == len(list(set(temp_pos_uid))), \
        "Number of extracted temporal positions and observed number of distinct temporal position identifiers differ"
    logging.info(f'\t There are {num_temp_pos} distinct temporal position uids')

    # Determine number of volumes
    if len(dcm_files) % num_temp_pos == 0:
        num_slices_per_vol = len(dcm_files) // num_temp_pos
    else:
        raise ValueError('Number of dcm files should be an integer multiple of number temporal positions!')
    logging.info(f'\t There are {num_slices_per_vol} slices corresponding to one temporal position uid')

    # Construct images
    width_index = 4
    num_temp_pos_str = str(num_temp_pos).zfill(width_index)
    for k in range(num_temp_pos):

        # Construct volume with itk reader
        reader = sitk.ImageSeriesReader()
        reader.SetFileNames([str(f) for f in dcm_files[k * num_slices_per_vol:(k+1) * num_slices_per_vol]])
        image = reader.Execute()

        # Write image to output
        writer = sitk.ImageFileWriter()
        writer.SetFileName(str(folder_out / f'{dcm_series_uid[0]}-{str(k+1).zfill(width_index)}-{num_temp_pos_str}.nrrd'))
        writer.UseCompressionOn()
        writer.Execute(image)


def main():

    logging.basicConfig(level=logging.INFO)

    # Command line parser
    parser = argparse.ArgumentParser(description="Convert dicom series to nrrd")
    parser.add_argument(
        'folder_main_in',
        help='Path to folder containing subfolders with dicom series',
    )
    parser.add_argument(
        'folder_main_out',
        help='Path to output folder where nrrd files will be stored',
    )

    # Construct paths from input
    args = parser.parse_args()
    folder_main_in = Path(args.folder_main_in)
    folder_main_out = Path(args.folder_main_out)

    if not folder_main_out.is_dir():
        folder_main_out.mkdir()

    # Convert dicom files
    subfolders = [x.stem for x in folder_main_in.iterdir() if x.is_dir()]
    logging.info(f'The following subfolders with dicom files were found {subfolders}')
    series_dict = construct_series_dict(folder_main_in)
    logging.info(f'{len(series_dict)} series were found')
    for uid in series_dict:
        logging.info(f'Converting dicom files with series id {uid} to nrrd')
        subfolder = folder_main_out / series_dict[uid]['file'][0].parent.stem
        if not subfolder.is_dir():
            subfolder.mkdir()
        subfolder_series = subfolder / str(series_dict[uid]['series_number'][0])    # Assuming same number should verify!
        if not subfolder_series.is_dir():
            subfolder_series.mkdir()
        dicom2nrrd(series_dict[uid]['file'], subfolder_series)


if __name__ == '__main__':
    main()