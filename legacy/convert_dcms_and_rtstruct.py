"""
Copyright (c) Nikita Moriakov, Jonas Teuwen and Ray Sheombarsing
This source code is licensed under the MIT license found in the
LICENSE file in the root directory of this source tree.
"""

import pydicom
import pathlib
import argparse
import itertools
import sys
import numpy as np
import SimpleITK as sitk
import skimage.draw

from image_readers import read_dcm_series

# Setup logging
# TODO: Move this to a central logger.
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

MODALITY_TAG = (0x8, 0x60)
REFERENCED_FRAME_OF_REFERENCE_SEQUENCE_TAG = (0x3006, 0x10)
FRAME_OF_REFERENCE_UID_TAG = (0x20, 0x52)
ROI_CONTOUR_SEQUENCE_TAG = (0x3006, 0x39)
ROI_DISPLAY_COLOR_TAG = (0x3006, 0x2a)
REFERENCED_ROI_NUMBER_TAG = (0x3006, 0x84)
ROI_NUMBER_TAG = (0x3006, 0x22)
CONTOUR_SEQUENCE_TAG = (0x3006, 0x40)
CONTOUR_DATA_TAG = (0x3006, 0x50)
ROI_NAME_TAG = (0x3006, 0x26)
STRUCTURE_SET_DATE_TAG = (0x3006, 0x8)
STRUCTURE_SET_TIME_TAG = (0x3006, 0x9)
STRUCTURE_SET_RIO_SEQUENCE_TAG = (0x3006, 0x20)

ROI = "gtv"


def grouper(iterable, n):
    """Given a long string it groups in pairs of `n`"""
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args)


def contour_world_to_index(contour_data, image):
    # TODO: This can be a property of a future Image class.
    return [image.TransformPhysicalPointToIndex(point) for point in
            [
                [float(_) for _ in group] for group in grouper(contour_data, 3)
            ]
    ]


def read_rtstruct(image_filenames, rtstruct_filename):
    """Reads directory of DICOM files and rstruct file"""
    output_dict = {'image_filenames': image_filenames}

    if rtstruct_filename:
        rtstruct = pydicom.read_file(str(rtstruct_filename), stop_before_pixels=True)
        if not rtstruct[MODALITY_TAG].value == 'RTSTRUCT':
            raise ValueError(f'{rtstruct_filename} is not an RTSTRUCT.')

        output_dict['rtstruct_filename'] = rtstruct_filename
    data, sitk_image, image_metadata = read_dcm_series(directory_or_files=image_filenames, series_id=None,
                                                       return_sitk=True)

    output_dict['num_slices'] = len(image_metadata['filenames'])
    output_dict['sitk_image'] = sitk_image

    if rtstruct_filename:
        # We start by constructing an empty dictionary with all available ROIs.
        structures = {
            structure[ROI_NUMBER_TAG].value: {
                'roi_name': structure[ROI_NAME_TAG].value,
                'points': [],
            } for structure in rtstruct[STRUCTURE_SET_RIO_SEQUENCE_TAG].value
        }
        if not structures:
            raise ValueError(f'{rtstruct_filename} does not contain any ROIs.')

        # Next, we fill the points
        for contour in rtstruct[ROI_CONTOUR_SEQUENCE_TAG].value:
            # Each ROI has a number.
            roi_no = contour[REFERENCED_ROI_NUMBER_TAG].value
            try:
                for contour_string in contour[CONTOUR_SEQUENCE_TAG].value:
                    # We can extract the string containing the contour information
                    contour_data = contour_string[CONTOUR_DATA_TAG].value
                    # Convert the contour data to points and store in the structure.
                    structures[roi_no]['points'] += contour_world_to_index(contour_data, sitk_image)
            except KeyError:
                # Ignore missing contours
                pass

        # The structures dictionary is slightly inconvenient, but this is
        # unfortunately how it is stored in the RTSTRUCT. We rewrite it here
        new_structures = {}
        output_dict['roi_names'] = []
        for roi_no, roi in structures.items():
            roi_name = roi['roi_name']
            roi_points = roi['points']

            if len(roi_points) < 3:  # Contour needs at least three points.
                continue
            output_dict['roi_names'].append(roi_name)
            new_structures[roi_name] = {
                'roi_number': roi_no,
                'points': roi_points,
            }
        output_dict['structures'] = new_structures

    return output_dict


def split_curves(curve, eps=1.5):
    """
    Traverse a Dicom world-curve and split up into closed contours. It is assumed that the
    individual contours can be detected in an unambiguous manner by traversing the world-curve in
    the order it is stored and locating the first point at which the world-curve comes back, i.e.,
    is close, to an initial starting point.
    Parameters
    ----------
    curve: float-valued numpy array of size [num_pts 2]
        the curve which traverses possibly multiple closed contours (coordinates are integers)
    eps: float
        threshold used to establish that two points are sufficiently close to each other. This
        threshold is used to mark the start and end of a curve.
    Returns
    -------
    contours: list of numpy arrays each of size [num_pts_contour 2]
        the extracted contours
    """

    # Initialization
    num_pts = curve.shape[1]
    current_start_pt = 0
    current_end_pt = 0

    # Find contours
    contours = []
    num_contours = 0
    while current_end_pt <= num_pts - 1:

        # Get out of epsilon neighborhood of starting point
        while current_end_pt <= num_pts - 1 and \
                np.linalg.norm(curve[:, current_start_pt] - curve[:, current_end_pt]) < eps:
            current_end_pt += 1

        # Locate endpoint contour and add to set of contours
        while current_end_pt <= num_pts - 1 and \
                np.linalg.norm(curve[:, current_start_pt] - curve[:, current_end_pt]) >= eps:
            current_end_pt += 1

        # Find start new contour
        while current_end_pt <= num_pts - 1 and \
                np.linalg.norm(curve[:, current_start_pt] - curve[:, current_end_pt]) < eps:
            current_end_pt += 1
        contours.append(curve[:, current_start_pt:current_end_pt])
        num_contours += 1
        current_start_pt = current_end_pt

    return contours


def world_curve_to_mask(curves, size):
    """
    Construct binary mask determined by a set of closed curves. A region enclosed by one curve
    contained in a region enclosed by another curve is interpreted as a hole. It is assumed that
    if two enclosed regions have a non-empty intersection, then one region is a proper subset of
    the other.
    Parameters
    ----------
    curve: float-valued numpy array of size [num_pts 2]
        the curve which traverses possibly multiple closed curves (coordinates are integers)
    size: 2-tuple of ints
        spatial dimensions of mask
    Returns
    -------
    mask: numpy array of prescribed size
        binary mask associated to region(s) enclosed by curves (should be stored as a binary array)
    """
    mask = np.zeros(size)

    for curve in split_curves(curves):
        rows, cols = skimage.draw.polygon(curve[0, :], curve[1, :])
        mask_curve = np.zeros(size)
        mask_curve[rows, cols] = 1
        intersect = np.where(mask * mask_curve == 1)
        new_pts = np.where((mask == 0) & (mask_curve == 1))
        if intersect:
            mask[intersect] = 0
        if new_pts:
            mask[new_pts] = 1

    return mask


class DicomRtstructReader(object):
    def __init__(self, image_filenames, rtstruct_filename):
        raw_dict = read_rtstruct(image_filenames, rtstruct_filename)
        self.roi_names = raw_dict.get('roi_names', None)
        self.num_slices = raw_dict['num_slices']

        self.image = raw_dict['sitk_image']
        self.structures = raw_dict.get('structures', [])

    def get_roi(self, roi_name):
        if roi_name not in self.roi_names:
            raise ValueError(f'ROI {roi_name} does not exist.')
        roi_dict = self.structures[roi_name]

        # Placeholder for ROI
        # TODO: Improve
        roi = np.zeros(
            (self.image.GetDepth(), self.image.GetHeight(), self.image.GetWidth()), dtype=np.uint8)

        for z, points in itertools.groupby(roi_dict['points'], key=lambda point: point[2]):
            points_list = list(points)
            y = [point[1] for point in points_list]
            x = [point[0] for point in points_list]
            roi[z, :, :] = world_curve_to_mask(np. array([y, x]), roi.shape[1::])

        sitk_roi = sitk.GetImageFromArray(roi)
        sitk_roi.SetSpacing(list(self.image.GetSpacing()))
        sitk_roi.SetDirection(self.image.GetDirection())
        sitk_roi.SetOrigin(self.image.GetOrigin())

        return sitk_roi


def construct_dict_study_instance(folder_in):
    """
    Construct dictionary StudyInstanceUID -> SeriesInstanceUID
    Parameters
    ----------
    folder_in : Path
        Path to folder containing dicom files.
    Returns
    -------
    dict
    """

    # Initialization
    dcm_files = list(folder_in.rglob('*.dcm'))
    num_dcm_files = len(dcm_files)
    logger.info(f'Located {num_dcm_files} dicom files.')
    study_dictionary = {}

    # Construct dictionary
    for idx, dicom_filename in enumerate(dcm_files):
        if (idx + 1) % (num_dcm_files // 10) == 0:
            logger.info(f'Working on {idx} / {num_dcm_files}.')

        dcm_object = pydicom.read_file(str(dicom_filename), stop_before_pixels=True)
        study_instance_uid = dcm_object.StudyInstanceUID
        if study_instance_uid not in study_dictionary:
            study_dictionary[study_instance_uid] = {}

        # If file is a RTSTRUCT append reference to associated series.
        if dcm_object.Modality in ['RTSTRUCT']:
            series_instance_uid = dcm_object.ReferencedFrameOfReferenceSequence[0] \
                                  .RTReferencedStudySequence[0] \
                                  .RTReferencedSeriesSequence[0] \
                                  .SeriesInstanceUID
            if series_instance_uid not in study_dictionary[study_instance_uid]:
                study_dictionary[study_instance_uid][series_instance_uid] = {}
            if 'RTSTRUCT' in study_dictionary[study_instance_uid][series_instance_uid]:
                study_dictionary[study_instance_uid][series_instance_uid]['RTSTRUCT'].append(str(dicom_filename))
            else:
                study_dictionary[study_instance_uid][series_instance_uid]['RTSTRUCT'] = [str(dicom_filename)]
            continue

        series_instance_uid = dcm_object.SeriesInstanceUID
        if series_instance_uid not in study_dictionary[study_instance_uid]:
            study_dictionary[study_instance_uid][series_instance_uid] = {}

        series_dictionary = study_dictionary[study_instance_uid][series_instance_uid]

        # Can be the case that only one file in the series has one of the following attributes,
        # however these always have to be set or are empty.
        for dicom_attribute in ['Modality', 'PatientID', 'PatientName']:
            if not series_dictionary.get(dicom_attribute, ''):
                series_dictionary[dicom_attribute] = str(getattr(dcm_object, dicom_attribute))

        if not series_dictionary.get('source_dir', None):
            series_dictionary['source_dir'] = dicom_filename.relative_to(folder_in).parent

        if 'filenames' in series_dictionary:
            series_dictionary['filenames'].append(str(dicom_filename))
        else:
            series_dictionary['filenames'] = [str(dicom_filename)]

    return study_dictionary


def parse_args():
    """Parse input arguments"""
    parser = argparse.ArgumentParser(description='Parse dicom folder and write to nrrd.')

    parser.add_argument( 
        'source_dir',
        type=pathlib.Path,
        help='root to source',
    )

    parser.add_argument(
        'target_dir',
        type=pathlib.Path,
        help='root to target',
    )

    parser.add_argument(
        '--keep-structure',
        action='store_true',
        help='If set, the folder structure will be maintained.',
    )

    return parser.parse_args()


def write_with_sitk(sitk_image, image_fn):
    if not image_fn.exists():
        sitk.WriteImage(sitk_image, str(image_fn), True)
    else:
        logger.info(f'{image_fn} exists. Skipping.')


def main():
    args = parse_args()
    print(args.target_dir, args.keep_structure)
    
    data_dictionary = construct_dict_study_instance(args.source_dir)
    if not args.keep_structure:
        raise NotImplementedError()

    for study_instance_uid in data_dictionary:
        study_dictionary = data_dictionary[study_instance_uid]
        for series_instance_uid in study_dictionary:
            logger.info(f'Processing {study_instance_uid} - {series_instance_uid}.')
            series_dictionary = study_dictionary[series_instance_uid]
            if 'RTSTRUCT' not in series_dictionary:
                continue

            root_dir = series_dictionary['source_dir']
            if len(series_dictionary['RTSTRUCT']) > 1:
                logger.error(f'Can only handle one RTSTRUCT: {root_dir}.')
                continue

            rtstruct_filename = series_dictionary['RTSTRUCT'][0]
            image_filenames = series_dictionary['filenames']

            patient_id = series_dictionary['PatientID']
            modality = series_dictionary['Modality']

            if args.keep_structure:
                write_to_folder = args.target_dir / root_dir
                logger.info(f'Writing to {write_to_folder}.')
                write_to_folder.mkdir(parents=True, exist_ok=True)

            image_fn = write_to_folder / f'{modality}.nrrd'
            if image_fn.exists():
                logger.info(f'{image_fn} exists. Skipping folder.')
                continue

            data = DicomRtstructReader(image_filenames, rtstruct_filename)
            logger.info(f'Processing {modality} data of patient {patient_id} with'
                        f' {len(data.roi_names)} structures.')

            structures = {}
            for roi_name in data.roi_names:
                if ROI in roi_name.lower():
                    structures[roi_name] = data.get_roi(roi_name)

            write_with_sitk(data.image,  image_fn)

            for roi_name in structures:
                write_with_sitk(
                    structures[roi_name], write_to_folder / f'{modality}-{roi_name}.nrrd')


if __name__ == '__main__':
    main()