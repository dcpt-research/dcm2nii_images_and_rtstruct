from __future__ import print_function

import os
import glob
import sys
import numpy as np

import dicom2nifti.compressed_dicom as compressed_dicom
import dicom2nifti.patch_pydicom_encodings

dicom2nifti.patch_pydicom_encodings.apply()

import gc
import os
import re
import traceback
import unicodedata
import pydicom
from pydicom.tag import Tag

import six
#from future.builtins import bytes
from six import iteritems

import dicom2nifti.common as common
import dicom2nifti.convert_dicom as convert_dicom
import dicom2nifti.settings
from dicom2nifti.exceptions import ConversionValidationError, ConversionError

# Setup logging
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

MODALITY_TAG = (0x8, 0x60)
REFERENCED_FRAME_OF_REFERENCE_SEQUENCE_TAG = (0x3006, 0x10)
FRAME_OF_REFERENCE_UID_TAG = (0x20, 0x52)
ROI_CONTOUR_SEQUENCE_TAG = (0x3006, 0x39)
ROI_DISPLAY_COLOR_TAG = (0x3006, 0x2a)
REFERENCED_ROI_NUMBER_TAG = (0x3006, 0x84)
ROI_NUMBER_TAG = (0x3006, 0x22)
CONTOUR_SEQUENCE_TAG = (0x3006, 0x40)
CONTOUR_DATA_TAG = (0x3006, 0x50)
ROI_NAME_TAG = (0x3006, 0x26)
STRUCTURE_SET_DATE_TAG = (0x3006, 0x8)
STRUCTURE_SET_TIME_TAG = (0x3006, 0x9)
STRUCTURE_SET_RIO_SEQUENCE_TAG = (0x3006, 0x20)

#ROI = "gtv"


def convert_volumes_by_directory(dicom_directory, output_folder, base_modality='ct', modalities = ['all'], compression=True, reorient=True):

    """
    This function will convert all dicom files to nifity images from directory, based on the choice of modalities. 

    # ? reference: This is a customed function based on dicom2nifti
    Args:
        dicom_directory ([type]): irectory with dicom files
        output_folder ([type]): folder to write the nifti files to
        base_modality (str, optional): reference modality for RTSTRUCT. Defaults to 'ct'.
        modalities (list, optional): list of modalites to convert. Defaults to ['all'].
        compression (bool, optional): if use gzip to create .nii.gz or .nii file. Defaults to True.
        reorient (bool, optional): reorient the dicoms according to LAS orientation. Defaults to True.

    Returns:
        base_modality_dcm_paths: list of paths for base modality files. 
    """
    #reorient = False
    # sort dicom files by series uid
    dicom_series = {}
    dicom_paths = {}
    base_modality_dcm_paths =[]

    for root, _, files in os.walk(dicom_directory):

        for dicom_file in files:
            file_path = os.path.join(root, dicom_file)


            # noinspection PyBroadException
            try:
                if compressed_dicom.is_dicom_file(file_path):
                    # read the dicom as fast as possible
                    # (max length for SeriesInstanceUID is 64 so defer_size 100 should be ok)



                    dicom_headers = compressed_dicom.read_file(file_path,
                                                               defer_size=100,
                                                               stop_before_pixels=False,
                                                               force=dicom2nifti.settings.pydicom_read_force)
                    if not _is_valid_imaging_dicom(dicom_headers):
                        logger.info("Skipping: %s" % file_path)
                        continue
                    logger.info("Organizing: %s" % file_path)
                    if dicom_headers.SeriesInstanceUID not in dicom_series:
                        dicom_series[dicom_headers.SeriesInstanceUID] = []
                    if dicom_headers.SeriesInstanceUID not in dicom_paths:
                        dicom_paths[dicom_headers.SeriesInstanceUID] = []
                        
                    dicom_series[dicom_headers.SeriesInstanceUID].append(dicom_headers)
                    dicom_paths[dicom_headers.SeriesInstanceUID].append(file_path)
            except:  # Explicitly capturing all errors here to be able to continue processing all the rest
                logger.warning("Unable to read: %s" % file_path)
                traceback.print_exc()

    # start converting one by one
    for series_id, dicom_input in iteritems(dicom_series):
        paths = dicom_paths[series_id]
        

        if modalities[0]!='all':
            if str(dicom_input[0].Modality).lower() not in modalities :
                continue

        base_filename = ""
        # noinspection PyBroadException
        try:
            # construct the filename for the nifti
            base_filename = ""
            if 'SeriesNumber' in dicom_input[0]:
                base_filename = _remove_accents('%s' % dicom_input[0].SeriesNumber)
                if 'Modality' in dicom_input[0]:
                    base_filename = _remove_accents('%s_%s' % (dicom_input[0].Modality, base_filename
                                                               ))
                    
                if 'SeriesDescription' in dicom_input[0]:
                    base_filename = base_filename + _remove_accents('_%s' % (dicom_input[0].SeriesDescription))
                    
                #elif 'SequenceName' in dicom_input[0]:
                #    base_filename = _remove_accents('%s_%s' % (base_filename,
                #                                               dicom_input[0].SequenceName))
                #elif 'ProtocolName' in dicom_input[0]:
                #    base_filename = _remove_accents('%s_%s' % (base_filename,
                #                                              dicom_input[0].ProtocolName))
            else:
                base_filename = _remove_accents(dicom_input[0].SeriesInstanceUID)
            logger.info('--------------------------------------------')
            logger.info('Start converting %s' % base_filename)
            short_path = os.path.basename(os.path.normpath(dicom_directory))
            print(f'{short_path} Start converting {base_filename}')
            if compression:
                nifti_file = os.path.join(output_folder, base_filename + '.nii.gz')
            else:
                nifti_file = os.path.join(output_folder, base_filename + '.nii')
                
            try:
                convert_dicom.dicom_array_to_nifti(dicom_input, nifti_file, reorient)
            except Exception as e:
                print(f'ERROR {e} on: {short_path} Start converting {base_filename}')

                if str(e) == 'SLICE_INCREMENT_INCONSISTENT':
                    dicom2nifti.settings.disable_validate_slice_increment()
                    dicom2nifti.settings.enable_resampling()
                    dicom2nifti.settings.set_resample_spline_interpolation_order(1)
                    dicom2nifti.settings.set_resample_padding(-1000)
                    dicom2nifti.settings.enable_pydicom_read_force()
                    
                    convert_dicom.dicom_array_to_nifti(dicom_input, nifti_file, reorient)
                    
                        
                    #raise ConversionValidationError('SLICE_INCREMENT_INCONSISTENT')
                elif str(e) == 'IMAGE_ORIENTATION_INCONSISTENT':
                    raise ConversionValidationError('IMAGE_ORIENTATION_INCONSISTENT')

            if str(dicom_input[0].Modality).lower() == str(base_modality).lower():
                #first_file_path.extend(paths)
                base_modality_dcm_paths = paths
                #print(first_file_path[:2])

            gc.collect()
        except:  # Explicitly capturing app exceptions here to be able to continue processing
            logger.info("Unable to convert: %s" % base_filename)
            traceback.print_exc()
            
    return base_modality_dcm_paths



def find_base_modality_path(dicom_directory, output_folder, base_modality='ct'):

    """
    This function will find the base modality paths for future RTSTRUCT conversion.
    Args:
        dicom_directory ([type]): irectory with dicom files
        output_folder ([type]): folder to write the nifti files to
        base_modality (str, optional): reference modality for RTSTRUCT. Defaults to 'ct'.
    Returns:
        base_modality_dcm_paths: list of paths for base modality files. 
    """

    # sort dicom files by series uid
    dicom_series = {}
    dicom_paths = {}
    base_modality_dcm_paths =[]
    first_file_path = []
    for root, _, files in os.walk(dicom_directory):
        for dicom_file in files:
            file_path = os.path.join(root, dicom_file)
            # noinspection PyBroadException
            try:
                if compressed_dicom.is_dicom_file(file_path):
                    # read the dicom as fast as possible
                    # (max length for SeriesInstanceUID is 64 so defer_size 100 should be ok)

                    #read the first dcm file


                    dicom_headers = compressed_dicom.read_file(file_path,
                                                               defer_size=100,
                                                               stop_before_pixels=False,
                                                               force=dicom2nifti.settings.pydicom_read_force)
                    if not _is_valid_imaging_dicom(dicom_headers):
                        logger.info("Skipping: %s" % file_path)
                        continue
                    logger.info("Organizing: %s" % file_path)
                    if dicom_headers.SeriesInstanceUID not in dicom_series:
                        dicom_series[dicom_headers.SeriesInstanceUID] = []
                    if dicom_headers.SeriesInstanceUID not in dicom_paths:
                        dicom_paths[dicom_headers.SeriesInstanceUID] = []
                        
                    dicom_series[dicom_headers.SeriesInstanceUID].append(dicom_headers)
                    dicom_paths[dicom_headers.SeriesInstanceUID].append(file_path)
            except:  # Explicitly capturing all errors here to be able to continue processing all the rest
                logger.warning("Unable to read: %s" % file_path)
                traceback.print_exc()

    # start converting one by one
    for series_id, dicom_input in iteritems(dicom_series):
        paths = dicom_paths[series_id]
        if str(dicom_input[0].Modality).lower() == str(base_modality).lower():
            first_file_path.extend(paths)
            base_modality_dcm_paths = first_file_path
            
    return base_modality_dcm_paths


def _is_valid_imaging_dicom(dicom_header):
    """
    Function will do some basic checks to see if this is a valid imaging dicom
    """
    # if it is philips and multiframe dicom then we assume it is ok
    try:
        if common.is_philips([dicom_header]):
            if common.is_multiframe_dicom([dicom_header]):
                return True

        if "SeriesInstanceUID" not in dicom_header:
            return False

        if "InstanceNumber" not in dicom_header:
            return False

        if "ImageOrientationPatient" not in dicom_header or len(dicom_header.ImageOrientationPatient) < 6:
            return False

        if "ImagePositionPatient" not in dicom_header or len(dicom_header.ImagePositionPatient) < 3:
            return False

        # for all others if there is image position patient we assume it is ok
        if Tag(0x0020, 0x0037) not in dicom_header:
            return False

        return True
    except (KeyError, AttributeError):
        return False


def _remove_accents(filename):
    """
    Function that will try to remove accents from a unicode string to be used in a filename.
    input filename should be either an ascii or unicode string
    """
    # noinspection PyBroadException
    try:
        filename = filename.replace(" ", "_")
        if isinstance(filename, type(six.u(''))):
            unicode_filename = filename
        else:
            unicode_filename = six.u(filename)
        cleaned_filename = unicodedata.normalize('NFKD', unicode_filename).encode('ASCII', 'ignore').decode('ASCII')

        cleaned_filename = re.sub('[^\w\s-]', '', cleaned_filename.strip().lower())
        cleaned_filename = re.sub('[-\s]+', '-', cleaned_filename)

        return cleaned_filename
    except:
        traceback.print_exc()
        return filename


def _remove_accents_(filename):
    """
    Function that will try to remove accents from a unicode string to be used in a filename.
    input filename should be either an ascii or unicode string
    """
    if isinstance(filename, type(six.u(''))):
        unicode_filename = filename
    else:
        unicode_filename = six.u(filename)
    valid_characters = bytes(b'-_.() 1234567890abcdefghijklmnopqrstuvwxyz')
    cleaned_filename = unicodedata.normalize('NFKD', unicode_filename).encode('ASCII', 'ignore')

    new_filename = six.u('')

    for char_int in bytes(cleaned_filename):
        char_byte = bytes([char_int])
        if char_byte in valid_characters:
            new_filename += char_byte.decode()

    return new_filename


def get_structures_by_directory(dicom_directory, skip_contours=False):
    """

    """
    contours = [] #  contours
    
    
    for root, _, files in os.walk(dicom_directory):
        for dicom_file in files:
            file_path = os.path.join(root, dicom_file)

            try:
                rt_struct_image = pydicom.read_file(file_path, force=True)
                
            except (IsADirectoryError, InvalidDicomError):
                raise InvalidFileFormatException('File {} is not an rt-struct dicom'.format(file_path))
                
            if not hasattr(rt_struct_image, 'StructureSetROISequence'):
                continue

                
            # first create a map so that we can easily trace referenced_roi_number back to its metadata
            metadata_mappings = {}
            for contour_metadata in rt_struct_image.StructureSetROISequence:
                
                metadata_mappings[contour_metadata.ROINumber] = contour_metadata

            for contour_sequence in rt_struct_image.ROIContourSequence:
                contour_data = {}

                metadata = metadata_mappings[contour_sequence.ReferencedROINumber] # retrieve metadata

                # I'm not sure if these attributes are always present in the metadata and contour_sequence
                # so I decided to write this in a defensive way.

                if hasattr(metadata, 'ROIName'):
                    contour_data['name'] = metadata.ROIName

                if hasattr(metadata, 'ROINumber'):
                    contour_data['roi_number'] = metadata.ROINumber

                if hasattr(metadata, 'ReferencedFrameOfReferenceUID'):
                    contour_data['referenced_frame'] = metadata.ReferencedFrameOfReferenceUID

                if hasattr(contour_sequence, 'ROIDisplayColor') and len(contour_sequence.ROIDisplayColor) > 0:
                    contour_data['display_color'] = contour_sequence.ROIDisplayColor

                if not skip_contours and hasattr(contour_sequence, 'ContourSequence') and len(contour_sequence.ContourSequence) > 0:
                    contour_data['sequence'] = []
                    for contour in contour_sequence.ContourSequence:
                        contour_data['sequence'].append({
                            'type': (contour.ContourGeometricType if hasattr(contour, 'ContourGeometricType') else 'unknown'),
                            'points': {
                                'x': ([contour.ContourData[index] for index in range(0, len(contour.ContourData), 3)] if hasattr(contour, 'ContourData') else None),  # this is just a fancy way to separate x, y, z from the rtstruct point array
                                'y': ([contour.ContourData[index + 1] for index in range(0, len(contour.ContourData), 3)] if hasattr(contour, 'ContourData') else None),  # this is just a fancy way to separate x, y, z from the rtstruct point array
                                'z': ([contour.ContourData[index + 2] for index in range(0, len(contour.ContourData), 3)] if hasattr(contour, 'ContourData') else None)   # this is just a fancy way to separate x, y, z from the rtstruct point array
                            }
                        })

                if contour_data:
                    # only add contour if we successfully extracted (some) data
                    contours.append(contour_data)

                     
    return contours



import os.path
import logging
import SimpleITK as sitk
import unicodedata
import re

class ContourOutOfBoundsException(Exception):
    pass

from skimage.draw import polygon

class DcmCoords2Mask():
    
    def _poly2mask(self, coords_x, coords_y, shape):
        fill_coords_x, fill_coords_y = polygon(coords_x, coords_y, shape)

        # check rtstruct
        if len(fill_coords_x) > 0 and len(fill_coords_y) > 0:
            if np.max(fill_coords_x) > 512 or np.max(fill_coords_y) > 512:
                raise Exception("The RTSTRUCT file is compromised")

        mask = np.zeros(shape, dtype=np.bool)
        mask[fill_coords_y, fill_coords_x] = True # sitk is xyz, numpy is zyx

        return mask

    def convert(self, rtstruct_contours, dicom_image, mask_background, mask_foreground):
        """
        convert patient coordinates to mask.
        """        
        shape = dicom_image.GetSize()

        mask = sitk.Image(shape, sitk.sitkUInt8)
        mask.CopyInformation(dicom_image)

        np_mask = sitk.GetArrayFromImage(mask)
        np_mask.fill(mask_background)

        for contour in rtstruct_contours:
            if contour['type'].upper() not in ['CLOSED_PLANAR', 'INTERPOLATED_PLANAR']:
                if 'name' in contour:
                    print(f'Skipping contour {contour["name"]}, unsupported type: {contour["type"]}')
                else:
                    print(f'Skipping unnamed contour, unsupported type: {contour["type"]}')
                continue

            coordinates = contour['points']

            pts = np.zeros([len(coordinates['x']), 3])

            for index in range(0, len(coordinates['x'])):
                # lets convert world coordinates to voxel coordinates
                world_coords = dicom_image.TransformPhysicalPointToIndex((coordinates['x'][index], coordinates['y'][index], coordinates['z'][index]))
                pts[index, 0] = world_coords[0]
                pts[index, 1] = world_coords[1]
                pts[index, 2] = world_coords[2]

            z = int(pts[0, 2])

            try:
                filled_poly = self._poly2mask(pts[:, 0], pts[:, 1], [shape[0], shape[1]])
                np_mask[z, filled_poly] = mask_foreground # sitk is xyz, numpy is zyx
                mask = sitk.GetImageFromArray(np_mask)
            except IndexError:
                # if this is triggered the contour is out of bounds
                raise ContourOutOfBoundsException()
            except RuntimeError as e:
                # this error is sometimes thrown by SimpleITK if the index goes out of bounds
                if 'index out of bounds' in str(e):
                    raise ContourOutOfBoundsException()
                raise e  # something serious is going on

        return mask



def name_normalize(filename):
    value = unicodedata.normalize('NFKD', filename).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip()
    value = re.sub('[-\s]+', '-', value)
    return value

#from dcmrtstruct2nii.adapters.convert.rtstructcontour2mask import DcmPatientCoords2Mask
#from dcmrtstruct2nii.adapters.input.contours.rtstructinputadapter import RtStructInputAdapter


def directory_dcm2nii_rtstruct2nii(dicom_directory, output_path, base_modality='ct', compression=True,  modalities=['all'], structures=None, mask_background_value=0, mask_foreground_value=1):
    """Converts DICOM modalities and DICOM RT Struct file to nii

        reference: dcmrtstruct2nii
    Args:
        dicom_directory ([type]): Path to the dicom file
        output_path ([type]): Output path where the masks are written to
        base_modality (str, optional): base modality for reference of rtstruct. Defaults to 'ct'.
        gzip (bool, optional): Optional, output .nii.gz if set to True. Defaults to True.
        modalities (list, optional): Modalities to convert. Defaults to ['all'].
        structures ([type], optional): strucutres to convert, if None convert all. Defaults to None.
        mask_background_value (int, optional): Mask background value. Defaults to 0.
        mask_foreground_value (int, optional): Foreground value. Defaults to 1.

    """    

    output_path = os.path.join(output_path, '')  # make sure trailing slash is there

    patient_folder = os.path.basename(os.path.normpath(dicom_directory))

    if mask_background_value < 0 or mask_background_value > 255:
        raise ValueError(f'Invalid value for mask_background_value: {mask_background_value}, must be between 0 and 255')

    if mask_foreground_value < 0 or mask_foreground_value > 255:
        raise ValueError(f'Invalid value for mask_foreground_value: {mask_foreground_value}, must be between 0 and 255')



    os.makedirs(output_path, exist_ok=True)

    #filename_converter = FilenameConverter()

    #convert all modalities to .nii.gz and save the base_modality(e.g. CT ) dcm paths.
    # ? image convert part. 
    dicom_file_names = find_base_modality_path(dicom_directory, output_path, base_modality)
    #dicom_file_paths = 
    convert_volumes_by_directory(dicom_directory, output_path, base_modality, modalities= modalities, compression=compression, reorient=False)
    
    # create a sitk dicom reader to get base modality dcm header infomation. 
    dicom_reader = sitk.ImageSeriesReader()
    

    dicom_reader.SetFileNames(sorted(dicom_file_names))
    
    # read base modality dcm images for rt structure extraction.
    dicom_image = dicom_reader.Execute()

    if structures is None:
        return
    
    #get all rt structures from dicom_directory. 
    rtstructs = get_structures_by_directory(dicom_directory)

    # dcm to mask converter
    dcm_coords_to_mask = DcmCoords2Mask()


    name = ''
    for rtstruct in rtstructs:
        struct_matcher = False
        name = name_normalize(rtstruct['name'])
        for st in structures:
            #print(f'searching for {st}, found ', rtstruct['name'])
            if st.lower() in name.lower():
                print(f'{patient_folder} searching for {st}, found ', rtstruct['name'])
                struct_matcher = True
                
        if structures[0]=='all' or struct_matcher :
            if not 'sequence' in rtstruct:
                #print(len(structures), struct_matcher)
                print(patient_folder,' Warning: Skipping mask {} no shape/polygon found'.format(rtstruct['name']))
                #print(f'length of {name} is {len(structures)}')
                continue

            print(patient_folder, '  Working on mask {}'.format(rtstruct['name']))
            try:
                mask = dcm_coords_to_mask.convert(rtstruct['sequence'], dicom_image, mask_background_value, mask_foreground_value)
            except ContourOutOfBoundsException:
                print(f'{patient_folder} Warnding: Structure {rtstruct["name"]} is out of bounds, ignoring contour!')
                continue

            # copy spacing information from dcm files
            mask.CopyInformation(dicom_image)

            mask_filename = name_normalize(f'mask_{rtstruct["name"]}')
            if compression:
                sitk.WriteImage(mask, f'{output_path}{mask_filename}' + '.nii.gz')
            else:
                sitk.WriteImage(mask, f'{output_path}{mask_filename}' + '.nii')


    print(f'Success!, {patient_folder} converted.')