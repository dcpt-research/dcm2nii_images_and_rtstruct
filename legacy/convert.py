import os
import glob
import numpy as np

import pydicom
from pydicom.tag import Tag
import os.path
import logging
import SimpleITK as sitk
from multiprocessing import Pool
import shutil


# from dcmrtstruct2nii.adapters.convert.rtstructcontour2mask import DcmPatientCoords2Mask
# from dcmrtstruct2nii.adapters.convert.filenameconverter import FilenameConverter
# from dcmrtstruct2nii.adapters.input.contours.rtstructinputadapter import RtStructInputAdapter
# from dcmrtstruct2nii.adapters.input.image.dcminputadapter import DcmInputAdapter

# from dcmrtstruct2nii.adapters.output.niioutputadapter import NiiOutputAdapter
# from dcmrtstruct2nii.exceptions import PathDoesNotExistException, ContourOutOfBoundsException

#from dicom2nii import *
from dicom2nii_from_directory import convert_volumes_by_directory, directory_dcm2nii_rtstruct2nii
from dicom2nifti.exceptions import ConversionValidationError, ConversionError
import dicom2nifti
import dicom2nifti.settings as settings

# import parser for .dcm to .nii.gz convertion.
from args import ConvertArgParser

def convert(inpath, outpath, base_modality='ct', compress=True, modalities = ['all'], rtstructs = None):
    
    if os.path.isdir(outpath):
        if args.mode:
            print(outpath)
            #shutil.rmtree(outpath) # ! use with caution
    else:
        os.mkdir(outpath)

    try:
        settings.validate_sliceincrement = True
        #convert_volumes_by_directory(inpath, outpath, compression=True, reorient=True)
        directory_dcm2nii_rtstruct2nii(inpath, outpath, base_modality=base_modality, 
                gzip=compress, modalities=modalities, structures=rtstructs)
        
    except ConversionValidationError as e:    
        if e == 'SLICE_INCREMENT_INCONSISTENT': 
            settings.disable_validate_slice_increment()
            settings.enable_resampling()
            settings.set_resample_spline_interpolation_order(1)
            settings.set_resample_padding(-1000)
            settings.enable_pydicom_read_force()
            
            print(f'ERROR: caught a SLICE_INCREMENT_INCONSISTENT with {outpath}, {e}')
            directory_dcm2nii_rtstruct2nii(inpath, outpath, base_modality=base_modality, 
                    gzip=compress, modalities=modalities, structures=rtstructs)
        elif e == 'IMAGE_ORIENTATION_INCONSISTENT':
            print(f'ERROR: caught a IMAGE_ORIENTATION_INCONSISTENT with {outpath}, {e}')

        logf.write(f"Failed to convert {str(outpath)}: {str(e)}\n")
        

    finally:
        # optional clean up code
        pass


def poolfunction(parameterlist):
    inpath = parameterlist[0]
    outpath = parameterlist[1]
    base_modality =  parameterlist[2]
    compress = parameterlist[3]
    modalities = parameterlist[4]
    rtstructs = parameterlist[5]
    convert(inpath, outpath, base_modality, compress, modalities, rtstructs)
    return


if __name__ == "__main__":
    """
    #use expample: ./dicom is args.in_locs, .dcm files are contained in each subfolder(patient) under ./dicom
    #nifity is out_loc 
    # │   ├── ./dicom
    # │   ├── ./dicom/HNCDL_001
    # │   ├── ./dicom/HNCDL_002
    #......
    #......
    # │   ├── ./nifity
    # │   ├── ./nifity/HNCDL_001
    # │   ├── ./nifity/HNCDL_002
    # │   ├── ./nifity/HNCDL_003
    #......
    """

    logf = open("convert.log", "w") # ! log function not working, need to optimize for multi-thread computing. 
    parser = ConvertArgParser()
    args = parser.parse_args()
    
    #over-write number of threads
    #args.nb_threads = 8

    #input output folds
    dicom_path = args.in_locs
    nii_path = args.out_loc
    print(f'The target rtstructures are:{args.rtstructs}')
    input_filepaths = sorted(glob.glob(os.path.join(dicom_path, '*')))
    output_filepaths =  [os.path.join(nii_path, os.path.basename(os.path.normpath(path)))  for path in input_filepaths]


    # single thread:
    if args.singlethread:
        for index, _ in enumerate(output_filepaths):
            convert(input_filepaths[index], output_filepaths[index],
                args.base_modality, args.compress, args.modalities, args.rtstructs)

    else: #multi-thread
        #create parameter list of input and output data 
        parameterlist = []
        for index, _ in enumerate(output_filepaths):
            parameterlist.extend([[input_filepaths[index], output_filepaths[index], 
                    args.base_modality, args.compress, args.modalities, args.rtstructs]])

        print(f'Found {len(parameterlist)} dicom folders, converting...')

        #make it multi-thread
        p = Pool(processes=args.nb_threads)     # default 8 processes.
        p.map(poolfunction, parameterlist)



    logf.close()

# ./HNCDL_049
# ./HNCDL_054
# ./HNCDL_055
# ./HNCDL_111
# ./HNCDL_117
# ./HNCDL_122
# ./HNCDL_135
# ./HNCDL_136
# ./HNCDL_262
# ./HNCDL_269
# ./HNCDL_318
# ./HNCDL_360
# ./HNCDL_369
# ./HNCDL_427
# ./HNCDL_428
# ./HNCDL_430
# ./HNCDL_431
# ./HNCDL_432
# ./HNCDL_434
# ./HNCDL_435
