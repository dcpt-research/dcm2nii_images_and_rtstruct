import argparse
import shutil
import pickle
import copy
import os
import numpy as np


class BaseArgParser(argparse.ArgumentParser):
    def __init__(self):
        super(BaseArgParser, self).__init__()

        self.parser = argparse.ArgumentParser()
        # add -f to fix errors when using jupyter notebook
        self.parser.add_argument('-f','--file',
                help='Path for input file. First line should contain number of lines to search in')      

    def namespace_to_dict(self, args):
        """Turns a nested Namespace object to a nested dictionary"""
        args_dict = vars(copy.deepcopy(args))

        for arg in args_dict:
            obj = args_dict[arg]
            if isinstance(obj, argparse.Namespace):
                args_dict[arg] = self.namespace_to_dict(obj)

        return args_dict

    def fix_nested_namespaces(self, args):
        """Makes sure that nested Namespace work. Supports only one level of nesting."""
        group_name_keys = []

        for key in args.__dict__:
            if '.' in key:
                group, name = key.split('.')
                group_name_keys.append((group, name, key))

        for group, name, key in group_name_keys:
            if group not in args:
                args.__dict__[group] = argparse.Namespace()

            args.__dict__[group].__dict__[name] = args.__dict__[key]
            del args.__dict__[key]

    def parse_args(self):
        args = self.parser.parse_args()
        args = self.namespace_to_dict(args)
        self.fix_nested_namespaces(args)
        return args


class ConvertArgParser(BaseArgParser):
    def __init__(self):
        super(ConvertArgParser, self).__init__()

        self.parser.add_argument('--in_locs', type=str,  default='C:\\data\\dicom',
                help='Mother path of all dicom folders.')
        self.parser.add_argument('--out_loc', type=str, default='C:\\data\\nifti',
                help='Location to write nifity data.')
                
        self.parser.add_argument('--info_loc', type=str, default='C:\\data\\',
                help='Location to write nifity data.')

        self.parser.add_argument('--modalities', type=str,  default='all',
                help='Comma-separated list of all input modalities to use, use lower cases.')
        self.parser.add_argument('--rtstructs', type=str, default='all',
                help='rtstructs/ROI to be converted. default all')

        self.parser.add_argument('--base_modality', type=str, default='ct',
                help='base dcm modality information for rtstructure.')

        #self.parser.add_argument('--crop_size', type=str, default='168,168,168',
        #        help='Crop size of image (comma-separated h,w,d).')

        self.parser.add_argument('--nb_threads', type=int, default=16,
                help='Number of threads be used for parallel computing.')

        self.parser.add_argument('--mode', type=bool, default=False,
                help='Mode of conversion, if to replace existed nifity folder.')

        self.parser.add_argument('--compress', type=bool, default=True,
                help='if to compress nifity files by gzip, defaut True: .nii.gz.')

        self.parser.add_argument('--singlethread', type=bool, default=False,
                help='if to run single thread.')


    def parse_args(self):
        args = self.parser.parse_args()

        # Create list of all input datasets.
        args.rtstructs = args.rtstructs.split(',')

        # Create list of all accepted modalities.
        args.modalities = args.modalities.split(',')

        if not os.path.exists(args.in_locs):
            raise Exception('ERROR: Dicom path does not exists.', args.in_locs)
        # Create output directory if it doesn't already exist.
        if os.path.isdir(args.out_loc):
            if args.mode:
                print(args.out_loc)
                shutil.rmtree(args.out_loc)
                os.mkdir(args.out_loc)
        else:
            os.mkdir(args.out_loc)


        return args
