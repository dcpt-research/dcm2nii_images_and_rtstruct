# Dicom to NIFTI from directory to directory

## Install dependency pakcages:
Please use python 3, optimal version >= 3.5

`pip install -r requirements.txt`

# !!! This guide is out dated, new version will be updated soon.

## ~~Useage example:~~

- Run with arguments

`python convert.py --in_locs /mnt/faststorage/hndata/data/dicom_test 
                --out_loc /mnt/faststorage/hndata/data/nifity_test1
                --modalities ct,pt,mr
                --rtstructs gtv
                --base_modality ct`

**!!! Causion: chose an empty folder for --out_loc, by defualt it will remove all the files under it to start.**


- Use default values, to convert all modalities and all rtstructs with spacing from CT.

`python convert.py --in_locs /mnt/faststorage/hndata/data/dicom_test 
                --out_loc /mnt/faststorage/hndata/data/nifity_test1
`

- Change arguments in ConvertArgParser(BaseArgParser) at 'args.py' and simply run:

`python convert.py`

## Input/Output folder structure
    example: -in_locs is ./dicom_test
    │   ├── ./dicom_test
    │   ├── ./dicom_test/HNCDL_001/*******.dcm
    ......
    │   ├── ./dicom_test/HNCDL_001/*******.dcm
    │   ├── ./dicom_test/HNCDL_002/*******.dcm
    ......
    
    example: -out_loc is ./nifity_test
    │   ├── ./nifity_test
    │   ├── ./nifity_test/HNCDL_001
    │   ├── ./nifity_test/HNCDL_002
    │   ├── ./nifity_test/HNCDL_003
    ......
