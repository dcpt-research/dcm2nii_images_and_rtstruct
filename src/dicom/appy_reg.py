
import pydicom
import pathlib
import argparse
import itertools
import sys
import numpy as np
import SimpleITK as sitk
import skimage.draw
import time

#---------------
# libs to normalize ROI names, used in remove_accents()
import re
import traceback
import six
import unicodedata
#---------------
from datetime import datetime

from image_readers import read_dcm_series
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def regster_with_dcm_reg_file(fixed_image: sitk.Image, moving_image: sitk.Image, dicom_registration=None,
                                    method=sitk.sitkBSpline):

    #sitk.sitkBSpline,  sitk.sitkLinear                
    """
    :param fixed_image: The fixed image
    :param moving_image: The moving image
    :param dicom_registration: the DICOM Registration file
    :param min_value: Value to put as background in resampled image
    :param method: interpolating method, recommend sitk.sitkLinear for images and sitk.sitkNearestNeighbor for masks
    :return:
    """                    
    assert type(dicom_registration) is pydicom.dataset.FileDataset \
           or dicom_registration is None, 'Pass dicom_registration as pydicom.read_file(reg_file) or None'

    min_value= np.min(sitk.GetArrayFromImage(moving_image)).astype(np.int8)
    print('min value:', min_value)

    affine_transform = sitk.AffineTransform(3)
    if dicom_registration is not None:
        registration_matrix = np.asarray(dicom_registration.RegistrationSequence[-1].MatrixRegistrationSequence[-1].
                                         MatrixSequence[-1].FrameOfReferenceTransformationMatrix).reshape((4, 4))
        registration_matrix = np.linalg.inv(registration_matrix)
        affine_transform.SetMatrix(registration_matrix[:3, :3].ravel())
        affine_transform.SetTranslation(registration_matrix[:3, -1])
    moving_resampled = sitk.Resample(moving_image, fixed_image, affine_transform, method, moving_image.GetPixelIDValue(),
                                     moving_image.GetPixelID())

    return moving_resampled


def main():
    folder_in = pathlib.Path('/mnt/faststorage/hndata/data_series/2_renamed/HNCDL_800/')
    sitk_ct = sitk.ReadImage('/mnt/faststorage/hndata/data_series/3_nifti_workplace/nifti/HNCDL_800/ct.nii.gz')
    sitk_t2 = sitk.ReadImage('/mnt/faststorage/hndata/data_series/3_nifti_workplace/nifti/HNCDL_800/mr_t2w.nii.gz')
    dcm_files = list(folder_in.rglob('*.dcm'))

    num_dcm_files = len(dcm_files)

    logger.info(f'Located {num_dcm_files} dicom files.')


    reg_object = None

    # Construct dictionary
    for idx, dicom_filename in enumerate(dcm_files):
        if (idx + 1) % (num_dcm_files // 10) == 0:
            logger.info(f'Working on {idx} / {num_dcm_files}.')

        dcm_object = pydicom.read_file(str(dicom_filename), stop_before_pixels=True)

        if dcm_object.Modality in ['REG']:
            reg_object = dcm_object

    new_t2 = regster_with_dcm_reg_file(sitk_ct, sitk_t2, dicom_registration=reg_object)
    sitk.WriteImage(new_t2, '/mnt/faststorage/hndata/data_series/3_nifti_workplace/nifti/HNCDL_800/mr_t2w_rigid.nii.gz')

if __name__ == '__main__':
    main()