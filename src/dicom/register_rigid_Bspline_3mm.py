import matplotlib.pyplot as plt
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/home/sysgen/gitlab/build/SimpleITK-build/Wrapping/Python')

import SimpleITK as sitk
import numpy as np
import os
import math
from scipy import signal
import shutil
import logging
import glob 

# basepath = os.path.normpath('/mnt/faststorage/hndata/dicom_new/3_nifti_workplace/nifti')
# out_basepath = os.path.normpath('/mnt/faststorage/hndata/dicom_new/3_nifti_workplace/nifti_reg_temp')

basepath = os.path.normpath('/data//hndata/nifty_raw')
out_basepath = os.path.normpath('/data/hndata/nifti_reg_3mm')
misc_dir = os.path.normpath('/data/hndata/misc') 

erode_filter = sitk.BinaryErodeImageFilter()
erode_filter.SetKernelType(sitk.sitkBall)
erode_filter.SetKernelRadius( 10 )
erode_filter.SetForegroundValue(1)
dilate_filter = sitk.BinaryDilateImageFilter()
dilate_filter.SetKernelType(sitk.sitkBall)
dilate_filter.SetKernelRadius( 5 )
dilate_filter.SetForegroundValue(1)

holefill_filter  = sitk.BinaryFillholeImageFilter()
holefill_filter.SetForegroundValue(1)

rigidonly = False

def rigidParameterMap():
    parameterMapRigid = sitk.GetDefaultParameterMap('rigid')
    
    parameterMapRigid['CheckNumberOfSamples']= ['false']
    parameterMapRigid['FixedImageDimension'] = ['3']
    parameterMapRigid['MovingImageDimension'] = ['3']
    parameterMapRigid['FixedInternalImagePixelType'] = ['float']
    parameterMapRigid['MovingInternalImagePixelType'] = ['float']
    
    parameterMapRigid['AutomaticScalesEstimation']= ['false']
    parameterMapRigid['AutomaticTransformInitialization']= ['true']
    parameterMapRigid['AutomaticTransformInitializationMethod']= ['GeometricalCenter']
    parameterMapRigid['DefaultPixelValue']= ['0.0']
    parameterMapRigid['FinalGridSpacingInVoxels']= ['10']
    parameterMapRigid['FixedImagePyramid']= ['FixedSmoothingImagePyramid']
    parameterMapRigid['HowToCombineTransforms']= ['Compose']
    parameterMapRigid['ImageSampler']= ['Random']
    parameterMapRigid['Interpolator']= ['BSplineInterpolator']
    parameterMapRigid['MaximumNumberOfIterations']= ['1024']
    parameterMapRigid['Metric']= ['AdvancedMattesMutualInformation']
    parameterMapRigid['MovingImagePyramid']= ['MovingSmoothingImagePyramid']
    parameterMapRigid['NumberOfResolutions']= ['3']
    parameterMapRigid['ImagePyramidSchedule']= ['4','4','2','2','1','1']
    parameterMapRigid['NumberOfSpatialSamples']=['1024']
    parameterMapRigid['Optimizer']= ['AdaptiveStochasticGradientDescent']
    parameterMapRigid['Registration']= ['MultiResolutionRegistration']
    parameterMapRigid['ResampleInterpolator']= ['FinalBSplineInterpolator']
    parameterMapRigid['Resampler']= ['DefaultResampler']
    parameterMapRigid['ResultImageFormat']= ['nii']
    parameterMapRigid['ResultImagePixelType']= ['unsigned short']
    # parameterMapRigid['Transform']= ['AffineTransform']
    parameterMapRigid['UseDirectionCosines']= ['true']
    parameterMapRigid['WriteResultImage']= ['true']    
    return parameterMapRigid

def BsplineParameterMap():
    parametermapBspline = sitk.GetDefaultParameterMap('bspline')
    parametermapBspline['CheckNumberOfSamples']= ['false']
    parametermapBspline['FixedImageDimension'] = ['3']
    parametermapBspline['MovingImageDimension'] = ['3']
    parametermapBspline['FixedInternalImagePixelType'] = ['float']
    parametermapBspline['MovingInternalImagePixelType'] = ['float']
    parametermapBspline['AutomaticScalesEstimation']= ['false']
    parametermapBspline['DefaultPixelValue']= ['0.0']
    parametermapBspline['FinalGridSpacingInPhysicalUnits']= ['12']
    parametermapBspline['FixedImagePyramid']= ['FixedSmoothingImagePyramid']
    parametermapBspline['HowToCombineTransforms']= ['Compose']
    parametermapBspline['ImageSampler']= ['Random']
    parametermapBspline['Interpolator']= ['BSplineInterpolator']
    parametermapBspline['MaximumNumberOfIterations']= ['512']
    parametermapBspline['Metric']= ["AdvancedMattesMutualInformation", "TransformRigidityPenalty"]
    parametermapBspline['Metric0Weight']=['2']
    parametermapBspline['Metric0Use']=['true']
    parametermapBspline['Metric1Weight']=['3']
    parametermapBspline['Metric1Use']=['true']
    parametermapBspline['MovingImagePyramid']= ['MovingSmoothingImagePyramid']
    # parametermapBspline['NumberOfResolutions']= ['3']
    # parametermapBspline['ImagePyramidSchedule']= ['4','4','4','2','2','2','1','1','1']
    # parametermapBspline['GridSpacingSchedule']=['4.0', '4.0', '4.0', '2.0', '2.0', '2.0', '1.0', '1.0', '1.0']
    parametermapBspline['NumberOfResolutions']= ['1']
    parametermapBspline['ImagePyramidSchedule']= ['1','1','1']
    parametermapBspline['GridSpacingSchedule']=['1.0', '1.0', '1.0']
    parametermapBspline['Optimizer']= ['AdaptiveStochasticGradientDescent']
    parametermapBspline['Registration']= ['MultiMetricMultiResolutionRegistration']
    parametermapBspline['ResampleInterpolator']= ['FinalBSplineInterpolator']
    parametermapBspline['Resampler']= ['DefaultResampler']
    parametermapBspline['ResultImageFormat']= ['nii']
    parametermapBspline['ResultImagePixelType']= ['unsigned short']
    parametermapBspline['Transform']= ['BSplineTransform']
    parametermapBspline['UseDirectionCosines']= ['true']
    parametermapBspline['WriteResultImage']= ['true']
    return parametermapBspline

def BsplineCombineParameterMap():
    parametermapBspline = sitk.GetDefaultParameterMap('bspline')
    parametermapBspline['CheckNumberOfSamples']= ['false']
    parametermapBspline['FixedImageDimension'] = ['3']
    parametermapBspline['MovingImageDimension'] = ['3']
    parametermapBspline['FixedInternalImagePixelType'] = ['float']
    parametermapBspline['MovingInternalImagePixelType'] = ['float']
    parametermapBspline['AutomaticScalesEstimation']= ['false']
    parametermapBspline['DefaultPixelValue']= ['0.0']
    parametermapBspline['FinalGridSpacingInPhysicalUnits']= ['12']
    parametermapBspline['FixedImagePyramid']= ['FixedRecursiveImagePyramid']
    parametermapBspline['HowToCombineTransforms']= ['Compose']
    parametermapBspline['ImageSampler']= ['Random']
    parametermapBspline['Interpolator']= ['BSplineInterpolator', 'BSplineInterpolator', 'BSplineInterpolator']
    parametermapBspline['MaximumNumberOfIterations']= ['600']
    parametermapBspline['Metric']= ["AdvancedMattesMutualInformation", "AdvancedMattesMutualInformation", "TransformRigidityPenalty"]
    parametermapBspline['Metric0Weight']=['2']
    parametermapBspline['Metric0Use']=['true']
    parametermapBspline['Metric1Weight']=['2']
    parametermapBspline['Metric1Use']=['true']
    parametermapBspline['Metric2Weight']=['1']
    parametermapBspline['Metric2Use']=['true']
    parametermapBspline['MovingImagePyramid']= ['MovingRecursiveImagePyramid', 'MovingRecursiveImagePyramid', 'MovingRecursiveImagePyramid']
    parametermapBspline['NewSamplesEveryIteration']=['true']
    parametermapBspline['NumberOfHistogramBins']=['64']
    parametermapBspline['NumberOfResolutions']= ['1']
    parametermapBspline['NumberOfSpatialSamples']= ['5000']
    parametermapBspline['ImagePyramidSchedule']= ['1','1','1']
    parametermapBspline['GridSpacingSchedule']=['4.0', '2.0', '1.0']
    parametermapBspline['Optimizer']= ['AdaptiveStochasticGradientDescent']
    parametermapBspline['Registration']= ['MultiMetricMultiResolutionRegistration']
    parametermapBspline['ResampleInterpolator']= ['FinalBSplineInterpolator']
    parametermapBspline['Resampler']= ['DefaultResampler']
    parametermapBspline['ResultImageFormat']= ['nii']
    parametermapBspline['ResultImagePixelType']= ['unsigned short']
    parametermapBspline['Transform']= ['BSplineTransform']
    parametermapBspline['UseDirectionCosines']= ['false']
    parametermapBspline['WriteResultImage']= ['true']
    return parametermapBspline

def getCropParameters(moving, marginsize=0):
    label_shape_filter = sitk.LabelShapeStatisticsImageFilter()
    label_shape_filter.Execute(moving)
    #this will give us a bounding box with [indexX, indexY, indexZ, sizeX, sizeY, sizeZ)
    bb = label_shape_filter.GetBoundingBox(1)

    # if you want to remove some additional area to be sure the corrupt parts are out add a crop margin
    margin = marginsize
    crop = np.arange(6)
    crop[0] = bb[0]+margin
    crop[1] = bb[0]+margin+bb[3]-2*margin
    crop[2] = bb[1]+margin
    crop[3] = bb[1]+margin+bb[4]-2*margin
    crop[4] = bb[2]+margin
    crop[5] = bb[2]+margin+bb[5]-2*margin
    #return values are thus [xmin, xmax, ymin, ymax, zmin, zmax]
    return crop

def getRegistrationMetric(fixed, moving):
    cropmr1 = getCropParameters(moving, 3)
    croppedmoving = moving[cropmr1[0]:cropmr1[1], cropmr1[2]:cropmr1[3], cropmr1[4]:cropmr1[5]]
    croppedfixed = fixed[cropmr1[0]:cropmr1[1], cropmr1[2]:cropmr1[3], cropmr1[4]:cropmr1[5]]
    croppedfixed = sitk.Cast(croppedfixed,sitk.sitkFloat32)
    croppedmoving = sitk.Cast(croppedmoving,sitk.sitkFloat32)
    registration_method = sitk.ImageRegistrationMethod()
    registration_method.SetMetricAsMattesMutualInformation()
    metric = registration_method.MetricEvaluate(croppedfixed,croppedmoving)
    return metric
    
parameterMapRigid= rigidParameterMap()
parameterMapBSpline = BsplineParameterMap()
parameterMapBSplineCombine = BsplineCombineParameterMap()

#for patient in subfolders:
    # basefolder = os.path.join(basepath, patient)

def combine_gtvt_gtvn(gtvt, gtvn):
    logger = logging.getLogger(__name__)
    img_npy1 = sitk.GetArrayFromImage(gtvt)
    try:
        img_npy2 = sitk.GetArrayFromImage(gtvn)
    except:
        img_npy2 = np.zeros_like(img_npy1)
        logger.info(f'GTVn not exist..')

    seg_new = np.zeros_like(img_npy1)

    seg_new[img_npy1 == 1] = 1
    seg_new[img_npy2 == 1] = 2

    seg_new = sitk.GetImageFromArray(seg_new)
    seg_new.CopyInformation(gtvt)

    return seg_new

def reslice_image(itk_image, itk_ref, is_label=False):
    resample = sitk.ResampleImageFilter()
    resample.SetReferenceImage(itk_ref)

    if is_label:
        resample.SetInterpolator(sitk.sitkNearestNeighbor)
    else:
        resample.SetInterpolator(sitk.sitkBSpline)

    return resample.Execute(itk_image)

def crop_save_nii(sitk_img, out_path, bbox, clip_pet = False):
    """
    crop image to bounding box,
    clip PET by 99 percentile of intensity.
    """
    np_img = sitk.GetArrayFromImage(sitk_img)#, (2, 1, 0))
    spacing = sitk_img.GetSpacing()
    origin = sitk_img.GetOrigin()
    z_abs,y_abs,x_abs = bbox[-2:], bbox[2:4], bbox[0:2]

    parent_dir = os.path.dirname(out_path)
    if not os.path.isdir(parent_dir):
        os.mkdir(parent_dir)

    # x = np.asarray((x_abs-origin[0])//spacing[0], dtype=np.int)
    # y = np.asarray((y_abs-origin[1])//spacing[1], dtype=np.int)
    # z = np.asarray((z_abs-origin[2])//spacing[2], dtype=np.int)

    if 'pet' in out_path.lower():
        #new_img = np_img[z[0]:z[1], y[0]:y[1], x[0]:x[1]]
        if clip_pet:
            print(f"PET clipped by 5 ") #{np.quantile(np_img, 0.995)}")
            np_img = np.clip(np_img, np_img.min(), 5) #np.quantile(np_img, 0.995))
    
    new_img = np_img[z_abs[0]:z_abs[1], y_abs[0]:y_abs[1], x_abs[0]:x_abs[1]]
    
    new_img = sitk.GetImageFromArray(new_img)
    #new_img.CopyInformation(sitk_img) 
    #print("bbox is > >>>", bbox[0], bbox[2], bbox[4])
    #new_img.SetOrigin = np.asarray([bbox[0], bbox[2], bbox[4]],dtype=np.float32) #np.asarray([bbox[0], bbox[2], bbox[4]])
    new_img.SetOrigin(np.asarray([bbox[0], bbox[2], bbox[4]], dtype=float)) #np.asarray([bbox[0], bbox[2], bbox[4]])
    #new_img.SetOutputDirection(sitk_img.GetDirection())
    new_img.SetSpacing(spacing)
    #print("origin is > >>>", new_img.GetOrigin())
    sitk.WriteImage(new_img, out_path)
    return new_img

def resample_image(itk_image, out_spacing=[1.0, 1.0, 1.0], is_label=False, refer_img = None):
    original_spacing = itk_image.GetSpacing()
    original_size = itk_image.GetSize()

    out_size = [
        int(np.round(original_size[0] * (original_spacing[0] / out_spacing[0]))),
        int(np.round(original_size[1] * (original_spacing[1] / out_spacing[1]))),
        int(np.round(original_size[2] * (original_spacing[2] / out_spacing[2])))
    ]

    resample = sitk.ResampleImageFilter()
    resample.SetOutputSpacing(out_spacing)
    resample.SetSize(out_size)
    resample.SetOutputDirection(itk_image.GetDirection())
    if refer_img != None:
        resample.SetOutputOrigin(refer_img.GetOrigin())
    else: 
        resample.SetOutputOrigin(itk_image.GetOrigin())
    resample.SetTransform(sitk.Transform())
    default_value = np.float64(sitk.GetArrayViewFromImage(itk_image).min())
    resample.SetDefaultPixelValue(default_value)# itk_image.GetPixelIDValue())

    if is_label:
        resample.SetInterpolator(sitk.sitkNearestNeighbor)
    else:
        resample.SetInterpolator(sitk.sitkBSpline) # sitkLinear)# 

    return resample.Execute(itk_image)

def register(patient, resample = False):
    ROIs = ["mandible", "paro", "brain", "chiasm", "cochlea", "esopha", "larynx", "lens", "lips", "optic", "oral", "spinal", "submand", "thyroid"]
    logger = logging.getLogger(__name__)
    logger.info(f'>>>>>>>processing {patient} >>>>>>>>>>>>>>>>>')
    basefolder = patient

    patientnr = os.path.basename(basefolder)
    all_nii_files = glob.glob(os.path.join(basefolder, '*'))

    oar_filenames = []

    for ROI in ROIs:
        for filenames in all_nii_files:
            if ROI in filenames.lower():
                oar_filenames.append(filenames)


    fixedfilename = os.path.join(basefolder, 'ct.nii.gz')
    pet_path = os.path.join(basefolder, 'pt.nii.gz')
    pet_suv_path = os.path.join(basefolder, 'pt_SUV.nii.gz')
    gtvt_path = os.path.join(basefolder, 'gtv_t.nii.gz')
    movingfilename = os.path.join(basefolder, 'mr_t1w_mdixona.nii.gz')
    movingfilename2 = os.path.join(basefolder, 'mr_t2w.nii.gz')


    if not(os.path.exists(fixedfilename)) or not(os.path.exists(movingfilename)) or not(os.path.exists(movingfilename2)) or not(os.path.exists(pet_path)) :
        return
    do_gtv = False

    if os.path.exists(gtvt_path):
        do_gtv = True

    regfolder = os.path.join(out_basepath, patientnr)
    outfolder = os.path.join(out_basepath, patientnr)
    oar_folder = os.path.join(outfolder, 'OARs')
    
    if not os.path.isdir(outfolder):
        os.makedirs(outfolder) 
    if not os.path.isdir(oar_folder):
        os.makedirs(oar_folder) 

    if do_gtv: 
        gtvn_path = os.path.join(basefolder, 'gtv_n.nii.gz')
        if not os.path.exists(gtvn_path):
            gtvn =  False
        else:
            gtvn = sitk.ReadImage(gtvn_path)

        gtvt = sitk.ReadImage(gtvt_path)
        gtv = combine_gtvt_gtvn(gtvt, gtvn)

    #print(patient)    
    ct = sitk.ReadImage(fixedfilename)
    pt = sitk.Cast(sitk.ReadImage(pet_path),sitk.sitkFloat32)
    pt_suv = sitk.Cast(sitk.ReadImage(pet_suv_path),sitk.sitkFloat32)
    
    moving = sitk.ReadImage(movingfilename)   
    moving2 = sitk.ReadImage(movingfilename2)


    ct_dst = os.path.join(outfolder, 'CT.nii.gz')
    pt_bqml_dst = os.path.join(outfolder, 'PET_BQML.nii.gz')
    pt_suv_dst = os.path.join(outfolder, 'PET.nii.gz')
    pt_clip_dst = os.path.join(outfolder, 'PETc.nii.gz')

    oar_imgs = {}
    if resample:
        ct  = resample_image(ct, out_spacing=[1.0, 1.0, 1.0], is_label=False)
        moving  = resample_image(moving, out_spacing=[1.0, 1.0, 1.0], is_label=False)
        moving2  = resample_image(moving2, out_spacing=[1.0, 1.0, 1.0], is_label=False)
        if do_gtv:
            gtv = resample_image(gtv, out_spacing=[1.0, 1.0, 1.0], is_label=True)
        for oar_filename in oar_filenames:
            oar_temp_img = sitk.ReadImage(oar_filename)
            oar_name = os.path.basename(oar_filename)
            oar_temp_img = resample_image(oar_temp_img, out_spacing=[1.0, 1.0, 1.0], is_label=True)
            oar_imgs[oar_name] = oar_temp_img
    else:
        for oar_filename in oar_filenames:
            oar_temp_img = sitk.ReadImage(oar_filename)
            oar_name = os.path.basename(oar_filename)
            oar_imgs[oar_name] = oar_temp_img
            
    #else: # no need to resample PET, reslice to CT is enough. 
        # need resample/s PET to CT
        #ct_spacing = ct.GetSpacing()
        # pt = resample_image(pt, out_spacing=ct_spacing, is_label=False, refer_img = ct)
        # pt_suv = resample_image(pt_suv, out_spacing=ct_spacing, is_label=False, refer_img = ct)
    
    pt = reslice_image(pt, ct, is_label=False)
    pt_suv = reslice_image(pt_suv, ct, is_label=False)

    fixed = ct

        # sitk.WriteImage(ct,  ct_dst)
        # sitk.WriteImage(pt,  pt_bqml_dst)
        # sitk.WriteImage(pt_suv,  pt_suv_dst)

    #sitk.WriteImage(gtv, os.path.join(outfolder, 'GTV.nii.gz'))

    regfolder = outfolder  
    
    sitk.WriteParameterFile(parameterMapRigid, os.path.join(regfolder, 'rigid_params.txt'))
    
    shutil.copy(__file__, os.path.join(regfolder, os.path.basename(__file__)))  # dst can be a folder; use shutil.copy2() to preserve timestamp
    # sitk.WriteImage(fixed, os.path.join(outfolder, 'fixed.nii.gz'))
    # sitk.WriteImage(moving, os.path.join(outfolder, 'moving.nii.gz'))
    # sitk.WriteImage(moving2, os.path.join(outfolder, 'moving2.nii.gz'))
    
    
    elastix = sitk.ElastixImageFilter()
    elastix.SetFixedImage(fixed)
    
    elastix.SetMovingImage(moving)
    elastix.LogToFileOff()
    elastix.SetOutputDirectory(regfolder)
    parameterMapRigid['NumberOfResolutions']= ['3']
    parameterMapRigid['ImagePyramidSchedule']= ['16','16','16','8','8', '8','4','4', '4']
    parameterMapRigid['AutomaticTransformInitializationMethod']= ['GeometricalCenter']
    parameterMapRigid['Metric']= ['AdvancedMattesMutualInformation']
    elastix.SetParameterMap(parameterMapRigid)
    
    resultImage = elastix.Execute()
    mask = resultImage>0
    resultImage = resultImage*sitk.Cast(mask, resultImage.GetPixelID()) 
    resultImage = sitk.Cast(resultImage,sitk.sitkUInt16)
    #sitk.WriteImage(resultImage, os.path.join(outfolder, 'T1rr.nii.gz'))
    #metric = getRegistrationMetric(fixed, resultImage)
    # print('GC', metric)

    bbox = getCropParameters(resultImage, 3)

    #crop_save_nii(resultImage, os.path.join(outfolder, 'T1rr.nii.gz'), bbox) 

    Transform = os.path.join(outfolder, 'TransformParameters.0.txt')
    rigidTransform = os.path.join(outfolder, 'TransformRigid.txt')
    if os.path.exists(rigidTransform):
        os.remove(rigidTransform)
    if os.path.exists(Transform):
        os.rename(Transform, rigidTransform)#rename
    
    fixed_mask = sitk.Cast(fixed>-200,sitk.sitkUInt8)
    fixed_mask = holefill_filter.Execute(fixed_mask)
    array = sitk.GetArrayFromImage(moving)
    threshold = np.average(array[array > 0])/2
    moving_mask = sitk.Cast(resultImage>threshold,sitk.sitkUInt8)
    moving_mask = holefill_filter.Execute(moving_mask)
    # sitk.WriteImage(fixed_mask, os.path.join(outfolder, 'fixed_mask.nii'))
    fixed_mask = fixed_mask + moving_mask
    fixed_mask = sitk.Cast(fixed_mask>1,sitk.sitkUInt8)
    dilate_filter.SetKernelRadius( 15 )
    fixed_mask = dilate_filter.Execute(fixed_mask)
    erode_filter.SetKernelRadius(12)
    fixed_mask = erode_filter.Execute(fixed_mask)
    #sitk.WriteImage(fixed_mask, os.path.join(outfolder, 'fixed_mask.nii.gz'))
    
    del elastix
    
    elastix = sitk.ElastixImageFilter()
    elastix.SetFixedImage(fixed)
    elastix.SetMovingImage(moving)
    elastix.AddMovingImage(moving2)
    elastix.AddMovingImage(moving)
    elastix.SetInitialTransformParameterFileName(rigidTransform)
    elastix.SetFixedMask(fixed_mask)
    elastix.LogToFileOff()
    elastix.SetOutputDirectory(regfolder)
    parameterMapRigid['ImagePyramidSchedule']= ['8','8', '8','4','4', '4', '2', '2','2']
    elastix.SetParameterMap(parameterMapRigid)
    if not(rigidonly):
        elastix.AddParameterMap(parameterMapBSplineCombine)
        sitk.WriteParameterFile(parameterMapBSplineCombine, os.path.join(regfolder, 'bspline_params.txt'))
    resultImage = elastix.Execute()
    mask = resultImage>0
    resultImage = resultImage*sitk.Cast(mask, resultImage.GetPixelID()) 
    resultImage = sitk.Cast(resultImage,sitk.sitkUInt16)
    metric_t1dr = getRegistrationMetric(fixed, resultImage)

    bbox = getCropParameters(resultImage, 3)

    if not(rigidonly):
        #sitk.WriteImage(resultImage, os.path.join(outfolder, 'T1dr.nii.gz'))
        crop_save_nii(resultImage, os.path.join(outfolder, 'T1dr.nii.gz'), bbox) 
    else:
        #sitk.WriteImage(resultImage, os.path.join(outfolder, 'T1rr2.nii.gz'))
        crop_save_nii(resultImage, os.path.join(outfolder, 'T1rr2.nii.gz'), bbox) 
    del elastix
    
    Transform = os.path.join(outfolder, 'TransformParameters.0.txt')
    rigidTransform2 = os.path.join(outfolder, 'TransformRigid2.txt')
    
    if os.path.exists(Transform):
        if os.path.exists(rigidTransform2):
            os.remove(rigidTransform2)
        os.rename(Transform, rigidTransform2)#rename
        
    
    if not(rigidonly):
        Transform = os.path.join(outfolder, 'TransformParameters.1.txt')
        BsplineTransform = os.path.join(outfolder, 'TransformBspline.txt')
        
        if os.path.exists(Transform):
            if os.path.exists(BsplineTransform):
                os.remove(BsplineTransform)
            os.rename(Transform, BsplineTransform)#rename
        
        
    transformix=sitk.TransformixImageFilter()
    transformix.SetMovingImage(moving2)
    parameter0=sitk.ReadParameterFile(rigidTransform)
    parameter1=sitk.ReadParameterFile(rigidTransform2)
    if not(rigidonly):
        parameter2=sitk.ReadParameterFile(BsplineTransform)
    
    transformix.SetTransformParameterMap(parameter0)
    transformix.Execute()
    resultImage = transformix.GetResultImage()
    mask = resultImage>0
    resultImage = resultImage*sitk.Cast(mask, resultImage.GetPixelID()) 
    resultImage = sitk.Cast(resultImage,sitk.sitkUInt16)
    #sitk.WriteImage(resultImage, os.path.join(outfolder, 'T2rr.nii.gz'))
    transformix.AddTransformParameterMap(parameter1)
    transformix.Execute()
    resultImage = transformix.GetResultImage()
    mask = resultImage>0
    resultImage = resultImage*sitk.Cast(mask, resultImage.GetPixelID()) 
    resultImage = sitk.Cast(resultImage,sitk.sitkUInt16)

    #sitk.WriteImage(resultImage, os.path.join(outfolder, 'T2rr2.nii.gz'))
    crop_save_nii(resultImage, os.path.join(outfolder, 'T2rr2.nii.gz'), bbox)     
    if not(rigidonly):
        transformix.AddTransformParameterMap(parameter2)
        transformix.Execute()
        resultImage = transformix.GetResultImage()
        mask = resultImage>0
        resultImage = resultImage*sitk.Cast(mask, resultImage.GetPixelID()) 
        resultImage = sitk.Cast(resultImage,sitk.sitkUInt16)
        #sitk.WriteImage(resultImage, os.path.join(outfolder, 'T2dr.nii.gz'))
        crop_save_nii(resultImage, os.path.join(outfolder, 'T2dr.nii.gz'), bbox)  
    del transformix
    if not(rigidonly):
        transformix2=sitk.TransformixImageFilter()
        transformix2.SetMovingImage(moving)
        transformix2.SetTransformParameterMap(parameter0)
        transformix2.AddTransformParameterMap(parameter1)
        transformix2.Execute()
        resultImage = transformix2.GetResultImage()
        mask = resultImage>0
        resultImage = resultImage*sitk.Cast(mask, resultImage.GetPixelID()) 
        resultImage = sitk.Cast(resultImage,sitk.sitkUInt16)
        #sitk.WriteImage(resultImage, os.path.join(outfolder, 'T1rr2.nii.gz'))
        crop_save_nii(resultImage, os.path.join(outfolder, 'T1rr2.nii.gz'), bbox)  
        del transformix2


    crop_save_nii(ct,  ct_dst, bbox) 
    crop_save_nii(pt,  pt_bqml_dst, bbox) 
    crop_save_nii(pt_suv,  pt_suv_dst, bbox)  
    if do_gtv:
        crop_save_nii(gtv,  os.path.join(outfolder, 'GTV.nii.gz'), bbox)    
    crop_save_nii(pt_suv,  pt_clip_dst, bbox, clip_pet=True)  

    for oar_key in oar_imgs.keys():
        oar_dst = os.path.join(oar_folder, oar_key)
        crop_save_nii(oar_imgs[oar_key],  oar_dst, bbox)  

    pt_mask = pt>0
    pt_mask = sitk.Cast(pt_mask,sitk.sitkUInt16)
    metric_pet = getRegistrationMetric(fixed, pt_mask)


    print("metric_pet is >>>", metric_pet)
    
    df  = pd.DataFrame(
        {
            'PatientID': patientnr,
            'x1': bbox[0],
            'x2': bbox[1],
            'y1': bbox[2],
            'y2': bbox[3],
            'z1': bbox[4],
            'z2': bbox[5],
            'pet2ct': metric_pet,
            't1dr2ct': metric_t1dr, #metric_pet, #
        }, index=[0])

    return df


from multiprocessing.pool import Pool
import pandas as pd
def main():

    #dfs_list = Manager().list()

    # basefolder = os.path.join(dirname, 'data', 'datascience','nifti', '1koskEvGaSUB9v2z')
    #old = sorted([ f.path for f in os.scandir(basepath) if f.is_dir() ])
    #print(old[:2])
    subfolders = sorted(list(set([ f.path for f in os.scandir(basepath) if f.is_dir() ])))
    #print(len(old), len(subfolders))
    #register('/mnt/faststorage/hndata/dicom_old/nifti/HNCDL_214')

    #res = register(subfolders[0])
    p = Pool(16)
    res = p.map(register,subfolders)
    dfs = pd.concat(res, ignore_index=True) 
    dfs.to_csv(os.path.join(misc_dir,'reg_metrics_bbox_3mm.csv'))

    ## p.map(register, subfolders)
    p.close()
    p.join()
if __name__ == '__main__':
    main()