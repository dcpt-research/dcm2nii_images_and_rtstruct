

import pydicom
import pathlib
import argparse
import itertools
import sys
import numpy as np
import SimpleITK as sitk
import skimage.draw
import time
import pandas as pd

#---------------
from datetime import datetime
import os
from image_readers import read_dcm_series

class Patient(object):
    def __init__(self, patient_dictionary, required_ROIs = ['gtv']):
        self.patient_dictionary = patient_dictionary
        self.primary = None
        self.secondary = None 

    def get_series_with_rtstruct(self):
        series_uid_list = []
        for series_instance_uid in self.patient_dictionary:
            series_dictionary = self.patient_dictionary[series_instance_uid]

        return series_uid_list

