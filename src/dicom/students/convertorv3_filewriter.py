"""
Convert dicom image modalities with ROI
The funcitons of this script is to use RTSTRUCT with SeriesInstanceUID to locate the
correponding primary image. And convert all image modalities sharing same StudyInstanceUID
with the primary image within a specific date gap (). 
Jintao Ren
jintaoren@onoclogy.au.dk
2022.01.06

"""

"""
Changes by datascience students spring 2022.
020522 -> split the processes into two steps: creating the dictionaries and writing the NIfTI-files.
          Represent the two steps in convertorv3_dictwriter and convertorv3_filewriter.
"""

import pydicom
import pathlib
import argparse
import itertools
import sys
import numpy as np
sys.path.insert(1, '/home/sysgen/gitlab/build/SimpleITK-build/Wrapping/Python')

import SimpleITK as sitk
import skimage.draw
import time
import os
import json
import pandas as pd
from multiprocessing.pool import Pool


#---------------
# libs to normalize ROI names, used in remove_accents()
import re
import traceback
import six
import unicodedata
#---------------
from datetime import datetime

# -*- coding: utf-8 -*-
#from pandas.io.json import json_normalize

from convertorv3_functions import wirte_primary_image_with_ROIs, write_secondary_images, read_dicts_from_excel
import ast

from image_readers import read_dcm_series

# Setup logging
# TODO: Move this to a central logger.
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

MODALITY_TAG = (0x8, 0x60)
REFERENCED_FRAME_OF_REFERENCE_SEQUENCE_TAG = (0x3006, 0x10)
FRAME_OF_REFERENCE_UID_TAG = (0x20, 0x52)
ROI_CONTOUR_SEQUENCE_TAG = (0x3006, 0x39)
ROI_DISPLAY_COLOR_TAG = (0x3006, 0x2a)
REFERENCED_ROI_NUMBER_TAG = (0x3006, 0x84)
ROI_NUMBER_TAG = (0x3006, 0x22)
CONTOUR_SEQUENCE_TAG = (0x3006, 0x40)
CONTOUR_DATA_TAG = (0x3006, 0x50)
ROI_NAME_TAG = (0x3006, 0x26)
STRUCTURE_SET_DATE_TAG = (0x3006, 0x8)
STRUCTURE_SET_TIME_TAG = (0x3006, 0x9)
STRUCTURE_SET_RIO_SEQUENCE_TAG = (0x3006, 0x20)   

def parse_args():
    """Parse input arguments"""
    parser = argparse.ArgumentParser(description='Parse dicom folder and write to nrrd.')

    #dynamic path section
    # dicom_dir = os.path.normpath('/mnt/faststorage/hndata/dicom_new/2_renamed')
    # nifti_dir = os.path.normpath('/mnt/faststorage/hndata/dicom_new/3_nifti_workplace/nifti/')  #os.path.join(grandparent_dir, "data", "nifti")
    # misc_dir = os.path.normpath('/mnt/faststorage/hndata/dicom_new/3_nifti_workplace/misc/') 

    dicom_dir = os.path.normpath('/mnt/faststorage/hndata/dicom/')
    nifti_dir = os.path.normpath('/mnt/faststorage/hndata/nifti')  #os.path.join(grandparent_dir, "data", "nifti")
    misc_dir = os.path.normpath('/mnt/faststorage/hndata/misc') 
    input_dir = misc_dir
    
    parser.add_argument( 
        '--source_dir',
        default = pathlib.Path(dicom_dir),
        type=pathlib.Path,
        help='root to source',
        #required=False
    )

    parser.add_argument(
        '--target_dir',
        default = pathlib.Path(nifti_dir),
        type=pathlib.Path,
        help='root to target',
        #required=False
    )

    # directory for miscellaneous output
    parser.add_argument(
        '--misc_dir',
        default = pathlib.Path(misc_dir),
        type = pathlib.Path,
        help = 'directory for miscellaneous output',
        #required = False
    )

    parser.add_argument(
        '--input_dir',
        default = pathlib.Path(input_dir),
        type = pathlib.Path,
        help = 'directory for miscellaneous input',
        #required = False
    )
    # parser.add_argument(
    #     '--keep-structure',
    #     action='store_true',
    #     help='If set, the folder structure will be maintained.',
    # )

    return parser.parse_args()

def write_series(series_instance_uid):
    args = parse_args()
    ROIs = ["mandible", "paro", "gtv", "brain", "chiasm", "cochlea", "esopha", "larynx", "lens", "lips", "optic", "oral", "spinal", "submand", "thyroid"]
    #ROIs = ["gtv"]
    logger.info(f'Writing NOT REPORTED - ')         #'{series_instance_uid}.')
    series_dictionary = series_instance_uid

    # Read image_filenames from .txt-file.
    image_filenames_txt = series_dictionary["filenames"]
    with open(image_filenames_txt) as f:
        image_filenames = f.read().splitlines()
    
    patient_id = series_dictionary['PatientID']
    modality_name = series_dictionary['Modality']
    series_description = series_dictionary['SeriesDescription']
    root_dir = series_dictionary['source_dir']
    
    write_to_folder = args.target_dir / root_dir
    
    if series_dictionary["bestrtstruct"]:
        
        rtstruct_filename = series_dictionary["bestrtstruct"]
        # Reinterpreting image_filenames from str to list

        wirte_primary_image_with_ROIs(modality_name, series_description, image_filenames, rtstruct_filename, ROIs, write_to_folder)
    else:
        write_secondary_images(modality_name, series_description, image_filenames, write_to_folder)

def main():

    args = parse_args()
    print(args.target_dir, )
    #modality_list = ['CT', 'MR', 'PT']
    #ROIs = ["mandible", "paro", "gtv", "brain", "chiasm", "cochlea", "esopha", "larynx", "lens", "lips", "optic", "oral", "spinal", "submand", "thyroid"]
    
    logger.debug("Reading parameters from input file.")
    #df_input = pd.read_csv(os.path.join(args.input_dir.__fspath__(),"filewriter_input.csv"),header=0)
    #logger.debug(f"Input parameters are: {df_input}.")
    
    logger.debug("Attempting to read dictionary files from (filepath!).")
    selected_patient_dictionary = read_dicts_from_excel(os.path.join(args.misc_dir.__fspath__(), "seriesdict_post_selection.xlsx"), 'false', 'false', 'false')#df_input["red"].iloc[0],df_input["orange"].iloc[0],df_input["yellow"].iloc[0])
    logger.debug(f"The read dictionary is {selected_patient_dictionary}. Please verify that this is correct.")
        
    #for series_instance_uid in selected_patient_dictionary:

    p = Pool(64)
    p.map(write_series, selected_patient_dictionary)
    p.close()
    p.join()
if __name__ == '__main__':
    main()