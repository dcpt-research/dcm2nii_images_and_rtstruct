# -*- coding: utf-8 -*-
"""
Created on Tue Jan  4 09:28:44 2022

@author: Jasnij
"""
#%%
import matplotlib.pyplot as plt
import SimpleITK as sitk
import numpy as np
import os
import math
from scipy import signal
import shutil
import sys

erode_filter = sitk.BinaryErodeImageFilter()
erode_filter.SetKernelType(sitk.sitkBall)
erode_filter.SetKernelRadius( 10 )
erode_filter.SetForegroundValue(1)
dilate_filter = sitk.BinaryDilateImageFilter()
dilate_filter.SetKernelType(sitk.sitkBall)
dilate_filter.SetKernelRadius( 5 )
dilate_filter.SetForegroundValue(1)

holefill_filter  = sitk.BinaryFillholeImageFilter()
holefill_filter.SetForegroundValue(1)

def rigidParameterMap():
    parameterMapaffine = sitk.GetDefaultParameterMap('rigid')
    
    parameterMapaffine['Metric']= ['AdvancedMattesMutualInformation']
    
    parameterMapaffine['SP_a'] = ['0.05']
    return parameterMapaffine

def main(iteration, patient_name):
    dirname = r'C:\Users\Andre\OneDrive\Skrivebord\Aarhus_Universitet\Dataprojekt'
    basefolder = os.path.join(dirname, 'nifti_for_reg', patient_name)

    parameterMapRigid= rigidParameterMap()
    #parameterMapaffine= affineParameterMap()

    fixedfilename = os.path.join(basefolder, 'ct.nii.gz')
    fixed = sitk.ReadImage(fixedfilename)
        
    outfolder = os.path.join(basefolder, 'NiftiReg-patient')
    outfolder = os.path.join(dirname, f'output/{patient_name}')
    
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)     
    regfolder = os.path.join(outfolder, 'Registration') 
    if not os.path.exists(regfolder):
        os.makedirs(regfolder)     

    sitk.WriteParameterFile(parameterMapRigid, os.path.join(regfolder, 'rigid.txt'))
    #sitk.WriteParameterFile(parameterMapaffine, os.path.join(regfolder, 'affine.txt'))
    #sitk.WriteParameterFile(parametermapBspline, os.path.join(regfolder, 'bspline.txt'))
    sitk.WriteImage(fixed, os.path.join(outfolder, 'fixed.nii'))

    fixed_mask = sitk.Cast(fixed>50,sitk.sitkUInt8)
    fixed_mask = holefill_filter.Execute(fixed_mask)
    # erode_filter.SetKernelRadius( 20)
    # fixed_mask = erode_filter.Execute(fixed_mask)
    dilate_filter.SetKernelRadius( 10 )
    fixed_mask = dilate_filter.Execute(fixed_mask)
    sitk.WriteImage(fixed_mask, os.path.join(outfolder, 'fixed_mask.nii'))
        
    movingfilename = os.path.join(basefolder, 'mr_t1w_mdixona.nii.gz')

    moving = sitk.ReadImage(movingfilename)
    sitk.WriteImage(moving, os.path.join(outfolder, 'moving.nii'))
    
    fixed_img = nib.load(os.path.join(outfolder, 'fixed.nii'))
    fixed_img_size = fixed_img.header.get_data_shape()[0:3]
    print(fixed_img_size)
    fixed_voxelx, fixed_voxely, fixed_voxelz = fixed_img.header.get_zooms()
    print(fixed_voxelx, fixed_voxely, fixed_voxelz)
    
    
    moving_img = nib.load(os.path.join(outfolder, 'moving.nii'))
    moving_img_size = moving_img.header.get_data_shape()[0:3]
    print(moving_img_size)
    moving_voxelx, moving_voxely, moving_voxelz = moving_img.header.get_zooms()
    
    same_size = np.array([700, 700, 700], dtype = "int")
    
    fixedx = same_size[0] / fixed_voxelx - fixed_img_size[0]
    fixedy = same_size[1] / fixed_voxely - fixed_img_size[1]
    fixedz = same_size[2] / fixed_voxelz - fixed_img_size[2]
    
    movingx = same_size[0] / moving_voxelx - moving_img_size[0]
    movingy = same_size[1] / moving_voxely - moving_img_size[1]
    movingz = same_size[2] / moving_voxelz - moving_img_size[2]
    
    #size_diff_fixed = same_size - np.array(fixed_img_size, dtype = "int")
    #size_diff_moving = same_size - np.array(moving_img_size, dtype = "int")
    
    assert min(fixedx, fixedy, fixedz) > 0, "One of the fixed sizes is greater than same_size"
    assert min(movingx, movingy, movingz) > 0, "One of the moving sizes is greater than same_size"
    
    #assert all(el % 2 == 0 for el in size_diff_fixed), "One of the fixed sizes is odd"
    #assert all(el % 2 == 0 for el in size_diff_moving), "One of the moving sizes is odd"  
    
    print(fixedy)
    
    fixedPadded = sitk.ConstantPad(fixed, (int(math.floor(fixedx / 2)), int(math.floor(fixedy / 2)), int(math.floor(fixedz / 2))),
                                          (int(math.ceil(fixedx / 2)), int(math.ceil(fixedy / 2)), int(math.ceil(fixedz / 2))), -1000)
    movingPadded = sitk.ConstantPad(moving, (int(math.floor(movingx / 2)), int(math.floor(movingy / 2)), int(math.floor(movingz / 2))),
                                          (int(math.ceil(movingx / 2)), int(math.ceil(movingy / 2)), int(math.ceil(movingz / 2))), 0)
    sitk.WriteImage(fixedPadded, os.path.join(outfolder, 'fixed.nii'))
    sitk.WriteImage(movingPadded, os.path.join(outfolder, 'moving.nii'))

    print("padded fixed with ", fixedx, fixedy, fixedz)
    print("Padded moving with ", movingx, movingy, movingz)

    elastixRigid = sitk.ElastixImageFilter()
    elastixRigid.SetFixedImage(fixedPadded)
    # elastixRigid.SetFixedMask(fixed_mask)
    elastixRigid.SetMovingImage(movingPadded)
    elastixRigid.LogToFileOn()
    
    # Create new folder for each patient to hold logs
    iterationfolder = os.path.join(dirname, 'nifti_for_reg', 'iterations')
    iterationlogfolder = os.path.join(iterationfolder, f'elastix{str(iteration)}-{patient_name}')
    if not os.path.exists(iterationlogfolder):
        os.makedirs(iterationlogfolder)  
    
    elastixRigid.SetOutputDirectory(iterationlogfolder)

    elastixRigid.SetParameterMap(parameterMapRigid)
    # elastixAffine.SetParameterMap(parameterMapaffine)
    # elastixAffine.AddParameterMap(parametermapBspline)

    resultImage = elastixRigid.Execute()
    sitk.WriteImage(resultImage, os.path.join(outfolder, 'patient-reg_R.nii'))
    outputTransform = os.path.join(iterationlogfolder, 'TransformParameters.0.txt')
    rigidTransform = os.path.join(iterationlogfolder, 'TransformParameters.rigid.txt')
    if os.path.exists(rigidTransform):
        os.remove(rigidTransform)
    os.rename(outputTransform, rigidTransform)#rename
    
    
    #os.rename(os.path.join(regfolder, 'elastix.log'), os.path.join(iterationfolder, f'elastix{str(iteration)}-{patient_name}.log'))
    

rootdir = r'C:\Users\Andre\OneDrive\Skrivebord\Aarhus_Universitet\Dataprojekt\nifti_for_reg'

import nibabel as nib
#img = nib.load(r'C:\Users\Andre\OneDrive\Skrivebord\Aarhus_Universitet\Dataprojekt\nifti_for_reg\0cxRxCE7BMtZlX7q\NiftiReg-patient\moving.nii')
#print(img.header.get_data_shape())

for dir in os.listdir(rootdir):
    print(dir)
    if dir != "iterations":
        main(0, dir)
    