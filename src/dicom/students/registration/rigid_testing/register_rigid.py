
#%%
import matplotlib.pyplot as plt
import SimpleITK as sitk
import numpy as np
import os
import math
from scipy import signal
import shutil


dirname = r'C:\Users\Andre\OneDrive\Skrivebord\Aarhus_Universitet\Dataprojekt\nifti_for_reg'
basefolder = os.path.join(dirname, '0cxRxCE7BMtZlX7q')



erode_filter = sitk.BinaryErodeImageFilter()
erode_filter.SetKernelType(sitk.sitkBall)
erode_filter.SetKernelRadius( 10 )
erode_filter.SetForegroundValue(1)
dilate_filter = sitk.BinaryDilateImageFilter()
dilate_filter.SetKernelType(sitk.sitkBall)
dilate_filter.SetKernelRadius( 5 )
dilate_filter.SetForegroundValue(1)

holefill_filter  = sitk.BinaryFillholeImageFilter()
holefill_filter.SetForegroundValue(1)

def rigidParameterMap():
    parameterMapaffine = sitk.GetDefaultParameterMap('rigid')
    
    parameterMapaffine['CheckNumberOfSamples']= ['false']
    parameterMapaffine['FixedImageDimension'] = ['3']
    parameterMapaffine['MovingImageDimension'] = ['3']
    parameterMapaffine['FixedInternalImagePixelType'] = ['float']
    parameterMapaffine['MovingInternalImagePixelType'] = ['float']
    
    parameterMapaffine['AutomaticScalesEstimation']= ['false']
    parameterMapaffine['AutomaticTransformInitialization']= ['true']
    parameterMapaffine['AutomaticTransformInitializationMethod']= ['GeometricalCenter']
    parameterMapaffine['DefaultPixelValue']= ['0.0']
    parameterMapaffine['FinalGridSpacingInVoxels']= ['10']
    parameterMapaffine['FixedImagePyramid']= ['FixedSmoothingImagePyramid']
    parameterMapaffine['HowToCombineTransforms']= ['Compose']
    parameterMapaffine['ImageSampler']= ['Random']
    parameterMapaffine['Interpolator']= ['BSplineInterpolator']
    parameterMapaffine['MaximumNumberOfIterations']= ['1024']
    parameterMapaffine['Metric']= ['AdvancedMattesMutualInformation']
    parameterMapaffine['MovingImagePyramid']= ['MovingSmoothingImagePyramid']
    parameterMapaffine['NumberOfResolutions']= ['3']
    parameterMapaffine['ImagePyramidSchedule']= ['4','4','2','2','1','1']
    parameterMapaffine['NumberOfSpatialSamples']=['1024']
    parameterMapaffine['Optimizer']= ['AdaptiveStochasticGradientDescent']
    parameterMapaffine['Registration']= ['MultiResolutionRegistration']
    parameterMapaffine['ResampleInterpolator']= ['FinalBSplineInterpolator']
    parameterMapaffine['Resampler']= ['DefaultResampler']
    parameterMapaffine['ResultImageFormat']= ['nii']
    parameterMapaffine['ResultImagePixelType']= ['unsigned short']
    # parameterMapaffine['Transform']= ['AffineTransform']
    parameterMapaffine['UseDirectionCosines']= ['true']
    parameterMapaffine['WriteResultImage']= ['true']    
    return parameterMapaffine


parameterMapRigid= rigidParameterMap()

fixedfilename = os.path.join(basefolder, 'ct.nii.gz')
fixed = sitk.ReadImage(fixedfilename)
for i in range(1,99):
    outfolder = os.path.join(basefolder,'Rigid'+str(i).zfill(2))
    if not os.path.isdir(outfolder):
        os.makedirs(outfolder) 
        break
         
# outfolder = os.path.join(basefolder,'Reg_21_07')
# if not os.path.exists(outfolder):
#     os.makedirs(outfolder)     
regfolder = outfolder  

sitk.WriteParameterFile(parameterMapRigid, os.path.join(regfolder, 'rigid.txt'))
# sitk.WriteParameterFile(parametermapBspline, os.path.join(regfolder, 'bspline.txt'))
shutil.copy(__file__, os.path.join(regfolder, os.path.basename(__file__)))  # dst can be a folder; use shutil.copy2() to preserve timestamp
sitk.WriteImage(fixed, os.path.join(outfolder, 'fixed.nii'))

# fixed_mask = sitk.Cast(fixed>200,sitk.sitkUInt8)
# fixed_mask = holefill_filter.Execute(fixed_mask)
# erode_filter.SetKernelRadius( 5)
# fixed_mask = erode_filter.Execute(fixed_mask)
# dilate_filter.SetKernelRadius( 15 )
# fixed_mask = dilate_filter.Execute(fixed_mask)
# sitk.WriteImage(fixed_mask, os.path.join(outfolder, 'fixed_mask.nii'))
    
movingfilename = os.path.join(basefolder, 'mr_t1w_mdixona.nii.gz')

moving = sitk.ReadImage(movingfilename)
sitk.WriteImage(moving, os.path.join(outfolder, 'moving.nii'))

elastixAffine = sitk.ElastixImageFilter()
elastixAffine.SetFixedImage(fixed)
# elastixAffine.SetFixedMask(fixed_mask)
elastixAffine.SetMovingImage(moving)
elastixAffine.LogToFileOn()
elastixAffine.SetOutputDirectory(regfolder)


elastixAffine.SetParameterMap(parameterMapRigid)
# elastixAffine.AddParameterMap(parametermapBspline)

resultImage = elastixAffine.Execute()
sitk.WriteImage(resultImage, os.path.join(outfolder, 'rigid_reg.nii'))

