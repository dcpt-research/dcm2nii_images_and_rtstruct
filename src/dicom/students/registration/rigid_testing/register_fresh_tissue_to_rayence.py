# -*- coding: utf-8 -*-
"""
Created on Tue Jan  4 09:28:44 2022

@author: Jasnij
"""
#%%
import matplotlib.pyplot as plt
import SimpleITK as sitk
import numpy as np
import os
import math
from scipy import signal

dirname = r'C:\Users\Andre\OneDrive\Skrivebord\Aarhus_Universitet\Dataprojekt'
basefolder = os.path.join(dirname, 'nifti_for_reg', '0ku1qeiExUvrl65s')

erode_filter = sitk.BinaryErodeImageFilter()
erode_filter.SetKernelType(sitk.sitkBall)
erode_filter.SetKernelRadius( 10 )
erode_filter.SetForegroundValue(1)
dilate_filter = sitk.BinaryDilateImageFilter()
dilate_filter.SetKernelType(sitk.sitkBall)
dilate_filter.SetKernelRadius( 5 )
dilate_filter.SetForegroundValue(1)

holefill_filter  = sitk.BinaryFillholeImageFilter()
holefill_filter.SetForegroundValue(1)

def rigidParameterMap():
    parameterMapaffine = sitk.GetDefaultParameterMap('rigid')
    
    parameterMapaffine['Metric']= ['AdvancedNormalizedCorrelation']
    
    return parameterMapaffine


def affineParameterMap():
    parameterMapaffine = sitk.GetDefaultParameterMap('affine')
    
    parameterMapaffine['CheckNumberOfSamples']= ['true']
    parameterMapaffine['FixedImageDimension'] = ['3']
    parameterMapaffine['MovingImageDimension'] = ['3']
    parameterMapaffine['FixedInternalImagePixelType'] = ['float']
    parameterMapaffine['MovingInternalImagePixelType'] = ['float']
    
    parameterMapaffine['AutomaticScalesEstimation']= ['false']
    parameterMapaffine['AutomaticTransformInitialization']= ['false']
    parameterMapaffine['DefaultPixelValue']= ['0.0']
    parameterMapaffine['FinalGridSpacingInVoxels']= ['10']
    parameterMapaffine['FixedImagePyramid']= ['FixedSmoothingImagePyramid']
    parameterMapaffine['HowToCombineTransforms']= ['Compose']
    parameterMapaffine['ImageSampler']= ['Random']
    parameterMapaffine['Interpolator']= ['BSplineInterpolator']
    parameterMapaffine['MaximumNumberOfIterations']= ['1024']
    parameterMapaffine['Metric']= ['AdvancedMattesMutualInformation']
    parameterMapaffine['MovingImagePyramid']= ['MovingSmoothingImagePyramid']
    parameterMapaffine['NumberOfResolutions']= ['3']
    parameterMapaffine['ImagePyramidSchedule']= ['4','4','4','2','2','2','1','1','1']
    parameterMapaffine['NumberOfSpatialSamples']=['1024']
    parameterMapaffine['Optimizer']= ['AdaptiveStochasticGradientDescent']
    parameterMapaffine['Registration']= ['MultiResolutionRegistration']
    parameterMapaffine['ResampleInterpolator']= ['FinalBSplineInterpolator']
    parameterMapaffine['Resampler']= ['DefaultResampler']
    parameterMapaffine['ResultImageFormat']= ['mhd']
    parameterMapaffine['ResultImagePixelType']= ['unsigned short']
    parameterMapaffine['Transform']= ['AffineTransform']
    parameterMapaffine['UseDirectionCosines']= ['true']
    parameterMapaffine['WriteResultImage']= ['true']    
    return parameterMapaffine

def BsplineParameterMap():
    parametermapBspline = sitk.GetDefaultParameterMap('bspline')
    parametermapBspline['CheckNumberOfSamples']= ['false']
    parametermapBspline['FixedImageDimension'] = ['3']
    parametermapBspline['MovingImageDimension'] = ['3']
    parametermapBspline['FixedInternalImagePixelType'] = ['float']
    parametermapBspline['MovingInternalImagePixelType'] = ['float']
    parametermapBspline['AutomaticScalesEstimation']= ['false']
    parametermapBspline['DefaultPixelValue']= ['0.0']
    parametermapBspline['FinalGridSpacingInPhysicalUnits']= ['12']
    parametermapBspline['FixedImagePyramid']= ['FixedSmoothingImagePyramid']
    parametermapBspline['HowToCombineTransforms']= ['Compose']
    parametermapBspline['ImageSampler']= ['Random']
    parametermapBspline['Interpolator']= ['BSplineInterpolator']
    parametermapBspline['MaximumNumberOfIterations']= ['100'] # 800 Jasper
    parametermapBspline['Metric']= ["AdvancedMattesMutualInformation", "TransformRigidityPenalty"]
    parametermapBspline['Metric0Weight']=['2']
    parametermapBspline['Metric0Use']=['true']
    parametermapBspline['Metric1Weight']=['1']
    parametermapBspline['Metric1Use']=['true']
    parametermapBspline['MovingImagePyramid']= ['MovingSmoothingImagePyramid']
    parametermapBspline['NumberOfResolutions']= ['2'] # 4
    # parametermapBspline['ImagePyramidSchedule']= ['8','8','8','4','4','4']
    parametermapBspline['ImagePyramidSchedule']= ['4','4','4','2','2','2'] # ['8','8','8','4','4','4','2','2','2','1','1','1']
    parametermapBspline['GridSpacingSchedule']=['4.0', '4.0', '4.0', '2.0', '2.0', '2.0'] # ['8.0', '8.0', '8.0', '4.0', '4.0', '4.0', '2.0', '2.0', '2.0', '1.0','1.0', '1.0']
    # parametermapBspline['GridSpacingSchedule']=['8.0', '8.0', '8.0', '4.0', '4.0', '4.0']
    parametermapBspline['Optimizer']= ['AdaptiveStochasticGradientDescent']
    parametermapBspline['Registration']= ['MultiMetricMultiResolutionRegistration']
    parametermapBspline['ResampleInterpolator']= ['FinalBSplineInterpolator']
    parametermapBspline['Resampler']= ['DefaultResampler']
    parametermapBspline['ResultImageFormat']= ['nii']
    parametermapBspline['ResultImagePixelType']= ['unsigned short']
    parametermapBspline['Transform']= ['BSplineTransform']
    parametermapBspline['UseDirectionCosines']= ['true']
    parametermapBspline['WriteResultImage']= ['true']
    return parametermapBspline

parameterMapRigid= rigidParameterMap()
#parameterMapaffine= affineParameterMap()
parametermapBspline=BsplineParameterMap()

fixedfilename = os.path.join(basefolder, 'ct.nii.gz')
fixed = sitk.ReadImage(fixedfilename)
    
outfolder = os.path.join(basefolder,'NiftiReg-patient-02')
if not os.path.exists(outfolder):
    os.makedirs(outfolder)     
regfolder = os.path.join(outfolder,'Registration') 
if not os.path.exists(regfolder):
    os.makedirs(regfolder)     

sitk.WriteParameterFile(parameterMapRigid, os.path.join(regfolder, 'rigid.txt'))
#sitk.WriteParameterFile(parameterMapaffine, os.path.join(regfolder, 'affine.txt'))
sitk.WriteParameterFile(parametermapBspline, os.path.join(regfolder, 'bspline.txt'))
sitk.WriteImage(fixed, os.path.join(outfolder, 'fixed.nii'))

fixed_mask = sitk.Cast(fixed>50,sitk.sitkUInt8)
fixed_mask = holefill_filter.Execute(fixed_mask)
# erode_filter.SetKernelRadius( 20)
# fixed_mask = erode_filter.Execute(fixed_mask)
dilate_filter.SetKernelRadius( 10 )
fixed_mask = dilate_filter.Execute(fixed_mask)
sitk.WriteImage(fixed_mask, os.path.join(outfolder, 'fixed_mask.nii'))
    
movingfilename = os.path.join(basefolder, 'mr_t1w_mdixona.nii.gz')

moving = sitk.ReadImage(movingfilename)
movingPadded = sitk.ConstantPad(moving, (128, 0, 0), (128, 0, 0))
sitk.WriteImage(movingPadded, os.path.join(outfolder, 'moving.nii'))

elastixRigid = sitk.ElastixImageFilter()
elastixRigid.SetFixedImage(fixed)
# elastixRigid.SetFixedMask(fixed_mask)
elastixRigid.SetMovingImage(movingPadded)
elastixRigid.LogToFileOn()
elastixRigid.SetOutputDirectory(regfolder)

elastixRigid.SetParameterMap(parameterMapRigid)
# elastixAffine.SetParameterMap(parameterMapaffine)
# elastixAffine.AddParameterMap(parametermapBspline)

resultImage = elastixRigid.Execute()
sitk.WriteImage(resultImage, os.path.join(outfolder, 'patient-02-reg_R.nii'))
outputTransform = os.path.join(regfolder, 'TransformParameters.0.txt')
rigidTransform = os.path.join(regfolder, 'TransformParameters.rigid.txt')
if os.path.exists(rigidTransform):
    os.remove(rigidTransform)
os.rename(outputTransform, rigidTransform)#rename

elastixBspline = sitk.ElastixImageFilter()
elastixBspline.SetFixedImage(fixed)
elastixBspline.SetFixedMask(fixed_mask)
elastixBspline.SetMovingImage(movingPadded)
elastixBspline.LogToFileOn()
elastixBspline.SetOutputDirectory(regfolder)
elastixBspline.SetInitialTransformParameterFileName(rigidTransform)

# elastixAffine.SetParameterMap(parameterMapRigid)
#elastixAffine.SetParameterMap(parameterMapaffine)
elastixBspline.SetParameterMap(parametermapBspline)

resultImage = elastixBspline.Execute()
sitk.WriteImage(resultImage, os.path.join(outfolder, 'patient-02-reg_R_A.nii'))
