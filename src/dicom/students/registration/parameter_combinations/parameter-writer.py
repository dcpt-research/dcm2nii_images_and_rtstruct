import json
import os
import itertools
import numpy as np

# --- USER SPECIFICATIONS ---

# Let user specify parameters
# If parameters are linked, specify them in tuple
parametersList = [
    ("NumberOfResolutions", "ImagePyramidSchedule"),
    "AutomaticTransformInitializationMethod"
]

# Let user specify valid parameter settings (should match with specified parameters respectively)
# If parameters are linked, specify them in tuple and zip before adding to parameterallvalues
parameter0values = (["2", "3"], [['4', '4', '4', '2', '2', '2'], ['4', '4', '4', '2', '2', '2', '1', '1', '1']])
parameter1values = ["GeometricalCenter", "CenterOfGravity"]
parameterallvalues = [list(zip(*parameter0values)),
                      parameter1values]

# --- END OF USER SPECIFICATIONS ---

combinationsList = list(itertools.product(*parameterallvalues))
print(combinationsList)
combinationsMedium = []

id = 0
for combination in combinationsList:
    for parameter in range(len(combination)):
        if type(combination[parameter]) == tuple:
            combinationsMedium.append([])

cwd = os.getcwd()
new_wd = os.path.join(cwd, os.pardir)
parameterFilePath = os.path.join(new_wd, 'nifti_for_reg')

data = {
    "combinations" : []
}




# Input combinationList into data-dictionary under "combinations"



data = {
    "combinations" : [
        {
            "id" : "1",
            "NumberOfResolutions" : "3",
            "ImagePyramidSchedule" : ['4', '4', '4', '2', '2', '2', '1', '1', '1'],
            "AutomaticTransformInitializationMethod" : "GeometricalCenter"
        },
        {
            "id" : "2",
            "NumberOfResolutions" : "3",
            "ImagePyramidSchedule" : ['4', '4', '4', '2', '2', '2', '1', '1', '1'],
            "AutomaticTransformInitializationMethod" : "CenterOfGravity"
        }
    ]
}

# Save chosen parameters in chosenparameters.csv



# Save data-dictionary in parameters.json
json_string = json.dumps(data)
#with open(os.path.join(parameterFilePath, 'parameters.json'), 'w') as f:
 #   json.dump(json_string, f)

