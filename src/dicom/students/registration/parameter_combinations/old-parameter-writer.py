import json
import os

# --- USER SPECIFICATIONS ---

# Let user specify parameters
parametersList = [
    "NumberOfResolutions",
    "ImagePyramidSchedule",
    "AutomaticTransformInitializationMethod"
]

# Let user specify valid parameter settings (should match with specified parameters respectively)
parameter0values = ["2", "3"]
parameter1values = [['4', '4', '4', '2', '2', '2'], ['4', '4', '4', '2', '2', '2', '1', '1', '1']]
parameter2values = ["GeometricalCenter", "CenterOfGravity"]
parameterallvalues = [parameter0values,
                      parameter1values,
                      parameter2values]

# Let user specify whether combination is linked, e.g. NumberOfResolutions = 3 does not
# work with ImagePyramidSchedule = ["2", "2", "2"]
#                   (row, col, entry) where row = parameter i, col = parameter ii
linkedCombinations = [[False, True, False],
                      [True, False, False],
                      [False, False, False]]

# --- END OF USER SPECIFICATIONS ---

cwd = os.getcwd()
new_wd = os.path.join(cwd, os.pardir)
parameterFilePath = os.path.join(new_wd, 'nifti_for_reg')

data = {
    "combinations" : []
}

combinationList = []
id = 0
# Iterate through combinations unless linked
for parameter_i in range(len(parameterallvalues)):
    for parameter_j in range(parameter_i, len(parameterallvalues)):
        if not linkedCombinations[parameter_i][parameter_j]:
            # Append combination to combinationList
            combinationList.append({"id" : id,
                                    parametersList[parameter_i]})

# Input combinationList into data-dictionary under "combinations"



data = {
    "combinations" : [
        {
            "id" : "1",
            "NumberOfResolutions" : "3",
            "ImagePyramidSchedule" : ['4', '4', '4', '2', '2', '2', '1', '1', '1'],
            "AutomaticTransformInitializationMethod" : "GeometricalCenter"
        },
        {
            "id" : "2",
            "NumberOfResolutions" : "3",
            "ImagePyramidSchedule" : ['4', '4', '4', '2', '2', '2', '1', '1', '1'],
            "AutomaticTransformInitializationMethod" : "CenterOfGravity"
        }
    ]
}

# Save chosen parameters in chosenparameters.csv



# Save data-dictionary in parameters.json
json_string = json.dumps(data)
with open(os.path.join(parameterFilePath, 'parameters.json'), 'w') as f:
    json.dump(json_string, f)

