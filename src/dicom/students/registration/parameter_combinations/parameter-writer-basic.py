import json
import os

cwd = os.getcwd()
new_wd = os.path.join(cwd, os.pardir)

parameterFilePath = os.path.join(new_wd, 'nifti_for_reg')

data = {
    "combinations" : [
        {
            "id" : "1",
            "NumberOfResolutions" : "3",
            "ImagePyramidSchedule" : ['4', '4', '4', '2', '2', '2', '1', '1', '1'],
            "AutomaticTransformInitializationMethod" : "GeometricalCenter"
        },
        {
            "id" : "2",
            "NumberOfResolutions" : "3",
            "ImagePyramidSchedule" : ['4', '4', '4', '2', '2', '2', '1', '1', '1'],
            "AutomaticTransformInitializationMethod" : "CenterOfGravity"
        }
    ]
}

json_string = json.dumps(data)

with open(os.path.join(parameterFilePath, 'parameters.json'), 'w') as f:
    json.dump(json_string, f)

