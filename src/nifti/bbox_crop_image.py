import SimpleITK as sitk


import os
from os.path import join
import numpy as np
import pandas as pd
import nibabel as nib

import csv
from scipy.ndimage import gaussian_filter
from scipy.ndimage.measurements import label
from scipy.ndimage.morphology import generate_binary_structure
from scipy.ndimage.measurements import center_of_mass
 

from find_bbox import *

if __name__ == "__main__":
    #num_workers = 8
    output_shape = (224,224,224)
    input_path = '/processing/j.ren/nki_hn/resampled/nki_1mm'
    output_path = '/processing/j.ren/nki_hn/cropped/nki_1mm'
    
    try:
        os.mkdir(output_path)
        print("Directory ", output_path, " Created ")
    except FileExistsError:
        print("Directory ", output_path, " already exists")


    patients = []
    for f in sorted(os.listdir(input_path)):
        patients.append(f)
    nfail = 0
    n=0
    list_auto_th = ['NKI_046', 'NKI_049', 'NKI_061', 'NKI_010']
    list_fix_bb = [] #['NKI_031']
    dict_fix_bb = {
        "NKI_031": np.asarray([-103.71998382,88.28001618,-309.73200226,-117.73200226,-127.41111946, 64.58888054])#,
        #"NKI_010": np.asarray([-175.16592765,48.83407235,-676.70934582, -452.70934582,-64.79718781,  205.20816326])
            }
    bb_df = pd.DataFrame(columns=['PatientID', 'x1', 'x2', 'y1', 'y2', 'z1', 'z2']) 
    fail_list = []


    for patient in patients:
        print('************* patient:', patient)
        in_path_ct = os.path.join(input_path,patient, 'CT.nii.gz')
        in_path_pt = os.path.join(input_path,patient, 'PET.nii.gz')
        in_path_t1c = os.path.join(input_path,patient, 'T1.nii.gz')
        in_path_t2 = os.path.join(input_path,patient, 'T2.nii.gz')
        in_path_gtvt_roi = os.path.join(input_path,patient, 'GTV.nii.gz')
        imgs = [] 
        img_names = ['CT.nii.gz', 'PET.nii.gz', 'T1.nii.gz', 'T2.nii.gz', 'GTV.nii.gz']

        if not os.path.exists(in_path_gtvt_roi):
            print('no GTVt')

        try: 
            img_ct = sitk.ReadImage(in_path_ct)
            img_pt = sitk.ReadImage(in_path_pt)
            img_t1c = sitk.ReadImage(in_path_t1c)
            img_t2 = sitk.ReadImage(in_path_t2)
            img_gtvt = sitk.ReadImage(in_path_gtvt_roi)

        except:
            fail_list.append(patient)
            print('cannot read ------------')
            continue
            
        px_spacing_ct = img_ct.GetSpacing()
        px_spacing_pt = img_pt.GetSpacing()
        px_origin_ct = img_ct.GetOrigin()
        px_origin_pt = img_pt.GetOrigin()

        np_ct = sitk.GetArrayFromImage(img_ct)
        gtvt = sitk.GetArrayFromImage(img_gtvt)

        auto_th = False
        if patient in list_auto_th:
            auto_th = False
        # Fix directly the bbox for some that don't work
        bbox = None
        if patient in list_fix_bb:
            bbox = dict_fix_bb[patient]

        #Direction RAI
        fail, bb = bbox_auto(img_pt, img_gtvt, img_t1c, px_spacing_ct, px_origin_ct, output_shape, auto_th=auto_th, bbox=bbox)
        if fail:
            print(f'{patient}, !!GTVt outside bbox -- {patient}')
            fail_list.append(patient)
        else:
            for img,name in zip([img_ct,img_pt,img_t1c,img_t2,img_gtvt], img_names):
                print(f"cropping: {patient} - {name}...")
                out = os.path.join(output_path,patient, name)
                crop_clip(img, out, bb)


        #bb, z_abs,y_abs,x_abs = bbox_auto_pet(img_pt, output_shape)
        nfail = nfail + fail
        n = n + 1
        bb_df = bb_df.append(
            {
                'PatientID': patient,
                'x1': bb[0],
                'x2': bb[1],
                'y1': bb[2],
                'y2': bb[3],
                'z1': bb[4],
                'z2': bb[5],
            },
            ignore_index=True)

    bb_df.to_csv(join(output_path,'../../bbox.csv'))


    fail_dict = {}
    fail_dict['PatientID'] = fail_list
    print ('fails/total',nfail,n)
    fail_df = pd.DataFrame(fail_dict)
    fail_df.to_csv(join(output_path,'../../fails.csv'))