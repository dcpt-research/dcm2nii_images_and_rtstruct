import numpy as np
import SimpleITK as sitk
from scipy.ndimage import gaussian_filter
from scipy.ndimage.measurements import label
import matplotlib.pyplot as plt
import pdb
from os.path import join
import numpy as np

from scipy.ndimage import gaussian_filter
from scipy.ndimage.measurements import label
from scipy.ndimage.morphology import generate_binary_structure
from scipy.ndimage.measurements import center_of_mass
from PIL import Image
import os

def bbox_auto_pet(sitk_pt, output_shape=(192, 192, 192), th=4, auto_th= False, fail=False):
    """Find a bounding box automatically based on the SUV

    Arguments:
        vol_pt {numpy array} -- The PET volume on which to compute the bounding box
        px_spacing_pt {tuple of float} -- The spatial resolution of the PET volume 
        px_origin_pt {tuple of float} -- The spatial position of the first voxel in the PET volume 

    Keyword Arguments:
        shape {tuple} -- The ouput size of the bounding box in millimeters
        th {float} -- [description] (default: {3})

    Returns:
        [type] -- [description]
    """
    np_pt = np.transpose(sitk.GetArrayFromImage(sitk_pt), (2, 1, 0))
    
    #np_pt = sitk.GetArrayFromImage(sitk_pt)

    px_spacing_pt = sitk_pt.GetSpacing()
    px_origin_pt = sitk_pt.GetOrigin()

    output_shape_pt = tuple(e1 / e2 for e1, e2 in zip(output_shape, px_spacing_pt))
    # Gaussian smooth
    np_pt_gauss = gaussian_filter(np_pt, sigma=3)
    #auto_th: based on max SUV value in the top of the PET scan
    if auto_th:
        th = np.max(np_pt[:, :, np.int(np_pt.shape[2] * 2 // 3):]) / 4
        print('auto_th = ', th)
    # OR fixed threshold
    np_pt_thgauss = np.where(np_pt_gauss > th, 1, 0)
    # Find brain as biggest blob AND not in lowest third of the scan
    labeled_array, _ = label(np_pt_thgauss)
    try:
        np_pt_brain = labeled_array == np.argmax(
            np.bincount(labeled_array[:,:,np_pt.shape[2] * 2 // 3:].flat)[1:]) + 1
    except:
        print('th too high?')
        # Quick fix just to pass for all cases
        th = 0.1
        np_pt_thgauss = np.where(np_pt_gauss > th, 1, 0)
        labeled_array, _ = label(np_pt_thgauss)
        np_pt_brain = labeled_array == np.argmax(
            np.bincount(labeled_array[:,:,np_pt.shape[2] * 2 // 3:].flat)[1:]) + 1
    # Find lowest voxel of the brain and box containing the brain
    z_min = np.min(np.argwhere(np.sum(np_pt_brain, axis=(0, 1))))
    z_max = np.max(np.argwhere(np.sum(np_pt_brain, axis=(0, 1))))

    #z = np.round(z_min/1.5)
    z = z_min
    #z = np.round((z_max + z_max)/2)
    #print(f"-----z is : {z}" )
    y1 = np.min(np.argwhere(np.sum(np_pt_brain, axis=(0, 2))))
    y2 = np.max(np.argwhere(np.sum(np_pt_brain, axis=(0, 2))))
    x1 = np.min(np.argwhere(np.sum(np_pt_brain, axis=(1, 2))))
    x2 = np.max(np.argwhere(np.sum(np_pt_brain, axis=(1, 2))))

    # Center bb based on this brain segmentation
    zshift = 30 // px_spacing_pt[2]
    if z - (output_shape_pt[2] - zshift) < 0:
        zbb = (0, output_shape_pt[2])
    elif z + zshift > np_pt.shape[2]:
        zbb = (np_pt.shape[2] - output_shape_pt[2], np_pt.shape[2])
    else:
        zbb = (z - (output_shape_pt[2] - zshift), z + zshift)

    yshift = 30 // px_spacing_pt[1]
    if np.int((y2 + y1) / 2 - yshift - np.int(output_shape_pt[1] / 2)) < 0:
        ybb = (0, output_shape_pt[1])
    elif np.int((y2 + y1) / 2 - yshift -
                np.int(output_shape_pt[1] / 2)) > np_pt.shape[1]:
        ybb = np_pt.shape[1] - output_shape_pt[1], np_pt.shape[1]
    else:
        ybb = ((y2 + y1) / 2 - yshift - output_shape_pt[1] / 2,
               (y2 + y1) / 2 - yshift + output_shape_pt[1] / 2)

    if np.int((x2 + x1) / 2 - np.int(output_shape_pt[0] / 2)) < 0:
        xbb = (0, output_shape_pt[0])
    elif np.int((x2 + x1) / 2 - np.int(output_shape_pt[0] / 2)) > np_pt.shape[0]:
        xbb = np_pt.shape[0] - output_shape_pt[0], np_pt.shape[0]
    else:
        xbb = ((x2 + x1) / 2 - output_shape_pt[0] / 2,
               (x2 + x1) / 2 + output_shape_pt[0] / 2)

    z_pt = np.asarray(zbb)
    y_pt = np.asarray(ybb)
    x_pt = np.asarray(xbb)

    if fail:
        z_pt = np.round((0, np_pt.shape[2]))
        print('using extrame PET window:', z_pt)

    # In the physical dimensions
    z_abs = z_pt * px_spacing_pt[2] + px_origin_pt[2]
    y_abs = y_pt * px_spacing_pt[1] + px_origin_pt[1]
    x_abs = x_pt * px_spacing_pt[0] + px_origin_pt[0]
    
    bb = np.asarray((x_abs, y_abs, z_abs)).flatten()
    return bb

def crop_clip(sitk_img, out_path, bbox):
    """
    crop image to bounding box,
    clip PET by 99 percentile of intensity.
    """
    np_img = sitk.GetArrayFromImage(sitk_img)#, (2, 1, 0))
    spacing = sitk_img.GetSpacing()
    origin = sitk_img.GetOrigin()
    z_abs,y_abs,x_abs = bbox[-2:], bbox[2:4], bbox[0:2]
    parent_dir = os.path.dirname(out_path)
    if not os.path.isdir(parent_dir):
        os.mkdir(parent_dir)
    
    y = np.asarray((y_abs-origin[1])//spacing[1],dtype=np.int)
    x = np.asarray((x_abs-origin[0])//spacing[0],dtype=np.int)
    z = np.asarray((z_abs-origin[2])//spacing[2],dtype=np.int)

    if 'PET' in out_path:
        print(f"PET clipped by {np.quantile(np_img, 0.99)}")
        np_img = np.clip(np_img, np_img.min(), np.quantile(np_img, 0.99))

    new_img = np_img[z[0]:z[1], y[0]:y[1], x[0]:x[1]]
    
    new_img = sitk.GetImageFromArray(new_img)
    new_img.SetOrigin = np.asarray([bbox[0], bbox[1], bbox[2]])
    new_img.SetSpacing(spacing)
    sitk.WriteImage(new_img, out_path)

def bbox_auto(sitk_pt, gtvt_img, img_t1c, px_spacing_ct, px_origin_ct, output_shape=(128, 128, 128), th = 3, auto_th = False, bbox=None, fail=False):
    """
    Use PET to define the brain, use brain location to get the center of x,y,z, and then get x y zrange based on image size.
    If PET z range not enough, use T1c image size to determine the z range. 
    """
    if bbox is not None:
        bb = bbox
    else:
        bb = bbox_auto_pet(sitk_pt, output_shape=output_shape, th=th, auto_th = auto_th, fail=fail)
    z_abs,y_abs,x_abs = bb[-2:], bb[2:4], bb[0:2]
    
    z_ct = np.asarray((z_abs-px_origin_ct[2])//px_spacing_ct[2],dtype=np.int)
    y_ct = np.asarray((y_abs-px_origin_ct[1])//px_spacing_ct[1],dtype=np.int)
    x_ct = np.asarray((x_abs-px_origin_ct[0])//px_spacing_ct[0],dtype=np.int)
    print("bb is: ", bb)
    print("x_ct, y_ct, z_ct is :", x_ct, y_ct, z_ct)
    gtvt = sitk.GetArrayFromImage(gtvt_img)
    if np.sum(gtvt[z_ct[0]:z_ct[1], y_ct[0]:y_ct[1], x_ct[0]:x_ct[1]]) != np.sum(gtvt):
        print("bbxo",np.sum(gtvt[z_ct[0]:-1, y_ct[0]:y_ct[1], x_ct[0]:x_ct[1]]),  "<<gtv:", np.sum(gtvt))
        #fail = True
        z_abs_t1c = get_z_from_t1c(img_t1c)
        print("using T1C")
        bb[-2] = z_abs_t1c[0]
        bb[-1] = z_abs_t1c[1]
        print("bb after:, ", bb)
    #In the CT resolution:
    z_ct = np.asarray((z_abs-px_origin_ct[2])//px_spacing_ct[2],dtype=np.int)
    y_ct = np.asarray((y_abs-px_origin_ct[1])//px_spacing_ct[1],dtype=np.int)
    x_ct = np.asarray((x_abs-px_origin_ct[0])//px_spacing_ct[0],dtype=np.int)

    # Check that the bbox contains the tumors

    #plt.imshow(sitk.GetArrayFromImage(img_t1c)[:,150,:])

    #while (fail == true):
    fail = False
    if z_ct[1] > gtvt.shape[0]:
        #print("! image too large on z!!!, ", z_ct)
        if np.sum(gtvt[z_ct[0]:-1, y_ct[0]:y_ct[1], x_ct[0]:x_ct[1]]) != np.sum(gtvt):
            print("<<<<bbxo",np.sum(gtvt[z_ct[0]:-1, y_ct[0]:y_ct[1], x_ct[0]:x_ct[1]]),  "<<gtv:", np.sum(gtvt))
            fail = True
    if z_ct[0] < 0:
        #print("! image too small on z!!!, ", z_ct)
        if np.sum(gtvt[0:z_ct[1], y_ct[0]:y_ct[1], x_ct[0]:x_ct[1]]) != np.sum(gtvt):
            print("<<<<bbxo",np.sum(gtvt[z_ct[0]:-1, y_ct[0]:y_ct[1], x_ct[0]:x_ct[1]]),  "<<gtv:", np.sum(gtvt))
            fail = True


    # Add the fails for which we had to change the threshold to keep track
    if auto_th:
        fail = True
    if fail:
        print(bb)
    return  fail, bb

def get_z_from_t1c(img_t1c):
    px_spacing_t1c = img_t1c.GetSpacing()
    px_origin_t1c = img_t1c.GetOrigin()
    np_t1c = np.transpose(sitk.GetArrayFromImage(img_t1c), (2, 1, 0))
    z_t1c = np.round((0, np_t1c.shape[2]))

    z_abs = z_t1c * px_spacing_t1c[2] + px_origin_t1c[2]
    return z_abs

def write_nii(wrt, img, path):
    wrt.SetFileName(path)
    wrt.Execute(img)

def check_singleGTVt(gtvt):
    s = generate_binary_structure(2,2)
    labeled_array, num_features = label(gtvt)
    if num_features !=1:
        print('num_features-------------------------------',num_features)
        print('number of voxels:')
        for i in np.unique(labeled_array)[1:]:
            print (np.sum(labeled_array==i))
        print('centers:')
        for i in np.unique(labeled_array)[1:]:
            print (center_of_mass(labeled_array==i))
    return 0