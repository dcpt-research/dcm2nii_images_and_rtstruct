
import matplotlib.pyplot as plt
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/home/sysgen/gitlab/build/SimpleITK-build/Wrapping/Python')

import SimpleITK as sitk
import numpy as np
import os

import shutil
import logging
import glob 

from multiprocessing.pool import Pool
import pandas as pd

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

def getRegistrationMetric(fixed, moving):
    # cropmr1 = getCropParameters(moving, 3)
    # croppedmoving = moving[cropmr1[0]:cropmr1[1], cropmr1[2]:cropmr1[3], cropmr1[4]:cropmr1[5]]
    # croppedfixed = fixed[cropmr1[0]:cropmr1[1], cropmr1[2]:cropmr1[3], cropmr1[4]:cropmr1[5]]
    fixed = sitk.Cast(fixed,sitk.sitkFloat32)
    moving = sitk.Cast(moving,sitk.sitkFloat32)
    registration_method = sitk.ImageRegistrationMethod()
    registration_method.SetMetricAsMattesMutualInformation()
    metric = registration_method.MetricEvaluate(fixed,moving)
    return metric



def calculate_reg_MI(subfolder):
    try:
        patientnr = os.path.basename(subfolder)
        compare_list = ['petc', 't1dr', 't2dr']
        file_list = sorted(glob.glob(os.path.join(subfolder, '*')))
        for file in file_list:
            if 'ct' in file.lower():
                fixed = sitk.ReadImage(file)
        metrics = {}
        metrics[patientnr] = {}
        for file in file_list:
            for modal in compare_list:
                if modal in file.lower():
                    moving = sitk.ReadImage(file)
                    metrics[patientnr]['PatientID'] = patientnr
                    metrics[patientnr][os.path.basename(file)] = np.round(getRegistrationMetric(fixed, moving), decimals=3) 

        df = pd.DataFrame.from_dict(metrics,
                            orient='index')

    except RuntimeError as e:
        logger.info(f'Processing {subfolder} - {e}.')
        return
    return df


def main():

    basepath = '/mnt/faststorage/hndata/nifti_reg_1mm'

    subfolders = sorted(list(set([ f.path for f in os.scandir(basepath) if f.is_dir() ])))

    with Pool(16) as p:
        res = p.map(calculate_reg_MI,subfolders)
    dfs = pd.concat(res, ignore_index=True) 
    dfs = dfs.set_index('PatientID')
    dfs.to_csv(os.path.join('/mnt/faststorage/hndata/misc','reg_metrics.csv'))


if __name__ == '__main__':
    main()