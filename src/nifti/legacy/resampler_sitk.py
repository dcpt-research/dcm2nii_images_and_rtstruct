import os
import glob
import SimpleITK as sitk
import pathlib
from multiprocessing import Pool
import tqdm


def resmaple(file):
    out_put_spacing = [1.0, 1.0, 1.0]
    
    basename  = os.path.basename(str(file))
    patient_id = file.parent.name
    out_path = '/processing/j.ren/nki_hn/resampled_itk/'
    out_path = os.path.join(out_path,patient_id)
    if not os.path.isdir(out_path):
        os.mkdir(out_path)

    out_path = os.path.join(out_path, basename)

    #out_path.mkdir()
    sitk_img = sitk.ReadImage(str(file))

    img = sitk.DICOMOrient(sitk_img, "LPS")
    #img.SetDirection((1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0))

    rs_filter = sitk.ResampleImageFilter()

    new_x = round(img.GetSize()[0] * img.GetSpacing()[0] / 1)
    new_y = round(img.GetSize()[1] * img.GetSpacing()[1] / 1)
    new_z = round(img.GetSize()[2] * img.GetSpacing()[2] / 1)

    rs_filter.SetOutputDirection(img.GetDirection())
    rs_filter.SetOutputOrigin(img.GetOrigin())
    #rs_filter.SetReferenceImage(img)
    rs_filter.SetOutputSpacing(out_put_spacing)
    rs_filter.SetSize([new_x, new_y, new_z])
    rs_filter.SetTransform(sitk.Transform())
    rs_filter.SetDefaultPixelValue(img.GetPixelIDValue())
    
    if 'gtv' in basename.lower():
        rs_filter.SetInterpolator(sitk.sitkNearestNeighbor)
    else:
        #rs_filter.SetInterpolator(sitk.sitkBSpline)
        rs_filter.SetInterpolator(sitk.sitkLinear)
    #rs_filter.SetDesiredCoordinateOrientation("RAI")
    img3 = rs_filter.Execute(img)

    sitk.WriteImage(img3, out_path)

if __name__ == '__main__':
    in_path = pathlib.Path('/processing/j.ren/nki_hn/selected/')
    files = sorted(list(in_path.rglob('*.nii.gz')))
    print(files)
    #files  = glob.glob(os.path.join(in_path, '*')) 

    pool = Pool(processes=32)      
    
    tqdm.tqdm(pool.map(resmaple, files[:40]), total=len(files))
    # for file in files: 
    #     resmaple(file)