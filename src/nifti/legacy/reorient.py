import os
import glob
import SimpleITK as sitk
import pathlib
from multiprocessing import Pool
import tqdm
import logging

def reorient(file):
    logger = logging.getLogger(__name__)
    logger.info(f'reorientation..{str(file)}')
    basename  = os.path.basename(str(file))
    patient_id = file.parent.name


    out_path = '/processing/j.ren/nki_hn/reorient/'
    out_path = os.path.join(out_path,patient_id)
    if not os.path.isdir(out_path):
        os.mkdir(out_path)

    out_path = os.path.join(out_path, basename)

    if 'PET_CT.nii.gz' in str(file):
        return 
    if 'CT_HR.nii.gz' in str(file):
        return
    #out_path.mkdir()
 
    if 'PET' in str(file):
        sitk_img = sitk.ReadImage(str(file), sitk.sitkFloat32)
    else:
        sitk_img = sitk.ReadImage(str(file), sitk.sitkInt16)

    try:
        img = sitk.DICOMOrient(sitk_img, "LPS")
        #img.SetDirection((1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0))
    except:
        return 
    sitk.WriteImage(img, out_path)
    logger.info('{} done'.format(patient_id))

if __name__ == '__main__':

    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logging.captureWarnings(True)
    
    in_path = pathlib.Path('/processing/j.ren/nki_hn/selected/')
    out_path = '/processing/j.ren/nki_hn/reorient/'
    # !!! deprecated !!!
    # deprecated - use resample.py to slice other modalities to CT. 

    # if not os.path.isdir(out_path):
    #     os.mkdir(out_path)

    # files = sorted(list(in_path.rglob('*.nii.gz')))

    # pool = Pool(processes=64)      
    # tqdm.tqdm(pool.map(reorient, files), total=len(files))

    # pool.close()
    # for file in files: 
    #     reorient(file)