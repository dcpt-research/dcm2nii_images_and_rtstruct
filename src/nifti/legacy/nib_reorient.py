# !!! deprecated !!!
import os
import glob
import pathlib
from multiprocessing import Pool
import tqdm
import logging
import nibabel as nib


def reorient(file):
    logger = logging.getLogger(__name__)
    logger.info(f'reorientation..{str(file)}')
    basename  = os.path.basename(str(file))
    patient_id = file.parent.name

    out_path = '/processing/j.ren/nki_hn/nib_reorient/'
    out_path = os.path.join(out_path,patient_id)
    if not os.path.isdir(out_path):
        os.mkdir(out_path)

    out_path = os.path.join(out_path, basename)

    if 'PET_CT.nii.gz' in str(file):
        return 
    if 'CT_HR.nii.gz' in str(file):
        return

    try:
        img = nib.load(str(file))
        re_img = nib.as_closest_canonical(img)
        nib.save(re_img, out_path)

    except Exception as exc:
        print(exc)
        logger.info('Unable to read image at {} '.format(patient_id))
        return

    logger.info('{} done'.format(patient_id))

if __name__ == '__main__':

    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logging.captureWarnings(True)
    # !!! deprecated !!!
    # deprecated - use resample.py to slice other modalities to CT. 

    # in_path = pathlib.Path('/processing/j.ren/nki_hn/selected/')
    # out_path = '/processing/j.ren/nki_hn/nib_reorient/'
    # if not os.path.isdir(out_path):
    #     os.mkdir(out_path)

    # files = sorted(list(in_path.rglob('*.nii.gz')))

    # pool = Pool(processes=32)      
    # pool.map(reorient, files)
    # pool.close()
    # for file in files: 
    #     reorient(file)
