"""
Jintao Ren
jintaoren@oncology.au.dk
2020.01.11
"""
import os 
import glob
import sys
import SimpleITK as sitk
import nibabel
import re
from multiprocessing import Pool
import tqdm
import pandas as pd
from shutil import copyfile
import shutil

from find_bbox import *


def _rename_patients(path):
    """
    rename pattients from raw_nii to with prefix 'NKI_xxx'
    store all images/GTV into a patient folder.
    """
    prefix = 'NKI_'
    out_path = '/processing/j.ren/nki_hn/all_raw_nii/'
    basename = os.path.basename(path)
    num = re.search(r'\d+', basename).group()

    patient_id = prefix + num.zfill(3)
    modality = basename.split(num+'_')[-1]

    output_dir = os.path.join(out_path, patient_id)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    output_image_path = os.path.join(output_dir, modality+'.gz')

    image = sitk.ReadImage(path)
    sitk.WriteImage(image, output_image_path)

def summarize_info(path):
    patient_dict = {}
    patient_id = os.path.basename(path)
    #['DIL_GTV2.nii.gz','DIL_gtv.nii.gz','DIL_GTVpet.nii.gz','PET.nii.gz','CT_3mm.nii.gz','DIL_GTV3.nii.gz','T1c.nii.gz','CT_HR.nii.gz','T1.nii.gz','PET_CT.nii.gz','DIL_GTV.nii.gz','DIL_GTV_larynx.nii.gz','T2.nii.gz']
    check_list = ['PET.nii.gz','CT_3mm.nii.gz','T1c.nii.gz','CT_HR.nii.gz','T1.nii.gz','PET_CT.nii.gz','T2.nii.gz','DIL_GTV.nii.gz']
    patient_dict['PatientID'] = patient_id
    for file in check_list:
        modality = file.split('.nii.gz')[0]
        patient_dict[modality] = 0
        if os.path.isfile(os.path.join(path, file)):
            patient_dict[modality] = 1
    
    return patient_dict

def select_patients(path):
    check_list = ['PET.nii.gz','CT_3mm.nii.gz','T1c.nii.gz','T2.nii.gz','DIL_GTV.nii.gz']
    patient_id = os.path.basename(path)

    selected = "/processing/j.ren/nki_hn/selected/"
    count = 0 
    for file in check_list:
        #modality = file.split('.nii.gz')[0]
        if os.path.isfile(os.path.join(path, file)):
            count +=1 

    dst = os.path.join(selected, patient_id)


    if count == len(check_list):
        # if not os.path.isdir(dst):
        #     os.mkdir(dst)
        if os.path.isdir(path):
            shutil.copytree(path, dst)
 
import random

def split_train_test(paths):
    random.seed(42)
    random.shuffle(paths)
    return paths

if __name__ == "__main__":
    num_workers = 64
    #1.---------_rename_patients----
    # base_path  = '/home/j.ren/faststorage/nki_hn/'
    # raw_path = base_path + 'raw_nii'
    # image_paths = sorted(glob.glob(os.path.join(raw_path, '*')))
    # p = Pool(processes=num_workers)        
    # #print(parameterlist)
    # p.map(_rename_patients, image_paths)
    # p.close()
    #------------------------------
    
    #2.---------summarize_information------
    # all_path = '/processing/j.ren/nki_hn/all_raw_nii/'

    # folder_names = sorted(glob.glob(os.path.join(all_path, '*')))
    # try:
    #     pool = Pool(processes=num_workers) 
    #     resultset = list(tqdm.tqdm(pool.imap(summarize_info, folder_names, chunksize=10), total=len(folder_names)))
    #     df = pd.DataFrame(resultset)
    #     #df = df.drop_duplicates()
    #     df.to_csv('/processing/j.ren/nki_hn/summary_report.csv')

    # except:
    #     print('There was a problem multithreading the Pool')
    #     raise
    #------------------------------

    #3.----------select_patients_based_on_modality_combinations-------
    # all_path = '/processing/j.ren/nki_hn/all_modalities/'
    # folder_names = sorted(glob.glob(os.path.join(all_path, '*')))
    # pool = Pool(processes=num_workers) 
    # pool.map(select_patients, folder_names)
    # pool.close()
    # for folder in folder_names:
    #     print(f"working on folder {folder}")
    #     select_patients(folder)

    #4.resemple image to either CT grid or isotropic 1mm : resample.py
    #5.crop image into 224*224*244 by lower brain from PET intensity : bbox_crop_image.py
    
    #-----------------------------------------------------------------
    #6.----------Split cropped images into Train and test  ----------- 
    # path = '/processing/j.ren/nki_hn/cropped/nki_CTgrid'
    # paths_out = '/processing/j.ren/nki_hn/cropped/nki_split_CTgrid'
    path = '/processing/j.ren/nki_hn/cropped/nki_1mm'
    paths_out = '/processing/j.ren/nki_hn/cropped/nki_split_1mm'

    paths = glob.glob(os.path.join(path, '*'))
    print("total patients: ", len(paths))

    shulled_list = split_train_test(sorted(paths))
    test_list = shulled_list[:40]
    train_list = shulled_list[40:]

    print("first 3 shuffled patients are: ", shulled_list[:3])

    train_path = os.path.join(paths_out, 'train')
    test_path = os.path.join(paths_out, 'test')

    if not os.path.isdir(train_path):
        os.mkdir(train_path)
    if not os.path.isdir(test_path):
        os.mkdir(test_path)

    for tests in test_list:
        patient_id = os.path.basename(tests)
        dst = os.path.join(test_path, patient_id)
        if os.path.isdir(tests):
            shutil.copytree(tests, dst)

    for trains in train_list:
        patient_id = os.path.basename(trains)
        dst = os.path.join(train_path, patient_id)
        if os.path.isdir(trains):
            shutil.copytree(trains, dst)
            
    #-----------------------------------------------------------------
