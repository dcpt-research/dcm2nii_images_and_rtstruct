import os
from multiprocessing import Pool
import glob
import nibabel as nib
import click
import logging
import pandas as pd
from nibabel import processing
import SimpleITK as sitk
import numpy as np

# Default paths
path_in = '/processing/j.ren/nki_hn/selected/'
path_out_1mm = '/processing/j.ren/nki_hn/resampled/nki_1mm/'
path_out_ct = '/processing/j.ren/nki_hn/resampled/nki_CTgrid/'



# def resampler(input_file, resampling, path_out_1mm, path_out_ct):
# """
# depcreated
# resampler with nibabel resample_from_to to interpolate multimodality
# images to CT. 

# """
#     logger = logging.getLogger(__name__)

#     patient = os.path.basename(str(input_file))
#     logger.info(f'resampling..{str(patient)}')

#     path_out_1mm = os.path.join(str(path_out_1mm), patient)
#     path_out_ct = os.path.join(str(path_out_ct), patient)

#     os.mkdir(path_out_1mm)
#     os.mkdir(path_out_ct)

#     try:
#         ct = nib.load(os.path.join(input_file, 'CT_3mm.nii.gz'))
#         t1c = nib.load(os.path.join(input_file, 'T1c.nii.gz'))
#         t2 = nib.load(os.path.join(input_file, 'T2.nii.gz'))
#         pet = nib.load(os.path.join(input_file, 'PET.nii.gz'))
#         gtv = nib.load(os.path.join(input_file, 'DIL_GTV.nii.gz'))
#     except:
#         logger.info(f'failed to load...{str(patient)}')
#         return

#     t1c_ctgrid= processing.resample_from_to(t1c, ct)
#     t2_ctgrid= processing.resample_from_to(t2, ct)
#     pet_ctgrid= processing.resample_from_to(pet, ct)

#     nib.save(t1c_ctgrid, os.path.join(path_out_ct, 'T1c.nii.gz'))  
#     nib.save(t2_ctgrid, os.path.join(path_out_ct, 'T2.nii.gz'))  
#     nib.save(pet_ctgrid, os.path.join(path_out_ct, 'PET.nii.gz'))  
#     nib.save(ct, os.path.join(path_out_ct, 'CT.nii.gz'))  
#     nib.save(gtv, os.path.join(path_out_ct, 'GTVt.nii.gz'))  

#     ct1mm = processing.resample_to_output(ct, resampling)
#     t1c_1mm = processing.resample_from_to(t1c, ct1mm)
#     t2_1mm = processing.resample_from_to(t2, ct1mm)
#     pet_1mm = processing.resample_from_to(pet, ct1mm)
#     gtv_1mm = processing.resample_from_to(gtv, ct1mm)

#     nib.save(t1c_1mm, os.path.join(path_out_1mm, 'T1c.nii.gz'))  
#     nib.save(t2_1mm, os.path.join(path_out_1mm, 'T2.nii.gz'))  
#     nib.save(pet_1mm, os.path.join(path_out_1mm, 'PET.nii.gz'))  
#     nib.save(ct1mm, os.path.join(path_out_1mm, 'CT.nii.gz'))  
#     nib.save(gtv_1mm, os.path.join(path_out_1mm, 'GTVt.nii.gz'))  


def check_gtvn(path):
    """
    check if GTVn exists in given folder path.
    if yes, return all the paths, else, return false.
    """
    gtvn_paths = glob.glob(os.path.join(path, 'DIL_GTVln*'))
    if len(gtvn_paths)== 0:
        return False
    else:
        return union_gtvn(gtvn_paths)

def union_gtvn(gtvn_paths):

    for idx, gtvn_path in enumerate(gtvn_paths):
        if idx ==0:
            img = sitk.ReadImage(gtvn_path)
            arr0 = sitk.GetArrayFromImage(img)
        else:
            arr = sitk.GetArrayFromImage(sitk.ReadImage(gtvn_path))
            arr0 = arr0 + arr

    arr0 = arr0 > 0
    arr0 = arr0.astype(np.int8)   
    img_new = sitk.GetImageFromArray(arr0)
    img_new.CopyInformation(img)

    return img_new

def combine_gtvt_gtvn(gtvt, gtvn):
    logger = logging.getLogger(__name__)
    img_npy1 = sitk.GetArrayFromImage(gtvt)
    try:
        img_npy2 = sitk.GetArrayFromImage(gtvn)
    except:
        logger.info(f'GTVn not exist..')

    seg_new = np.zeros_like(img_npy1)

    seg_new[img_npy1 == 1] = 1
    seg_new[img_npy2 == 1] = 2

    seg_new = sitk.GetImageFromArray(seg_new)
    seg_new.CopyInformation(gtvt)

    return seg_new

def resampler_sitk(input_file, path_out_1mm, path_out_ct):
    logger = logging.getLogger(__name__)

    patient = os.path.basename(str(input_file))
    logger.info(f'resampling..{str(patient)}')

    path_out_1mm = os.path.join(str(path_out_1mm), patient)
    path_out_ct = os.path.join(str(path_out_ct), patient)

    
    try:
        ct = sitk.ReadImage(os.path.join(input_file, 'CT_3mm.nii.gz'))
        t1 = sitk.ReadImage(os.path.join(input_file, 'T1c.nii.gz'))
        t2 = sitk.ReadImage(os.path.join(input_file, 'T2.nii.gz'))
        pet = sitk.ReadImage(os.path.join(input_file, 'PET.nii.gz'))
        gtvt = sitk.ReadImage(os.path.join(input_file, 'DIL_GTV.nii.gz'))
    except:
        logger.info(f'failed to load...{str(patient)}')
        return

    gtvn = check_gtvn(input_file)


    # Resample to 1mm isotropic resolution
    ct_1mm = resample_image(ct)
    pet_1mm = reslice_image(pet, ct_1mm )
    t1_1mm = reslice_image(t1, ct_1mm)
    t2_1mm = reslice_image(t2, ct_1mm)
    gtvt_1mm = reslice_image(gtvt, ct_1mm, is_label = True)


    pet_3mm = reslice_image(pet, ct )
    t1_3mm = reslice_image(t1, ct)
    t2_3mm = reslice_image(t2, ct)

    os.mkdir(path_out_1mm)
    os.mkdir(path_out_ct)



    try:
        sitk.WriteImage(ct_1mm, os.path.join(path_out_1mm, 'CT.nii.gz'))
        sitk.WriteImage(pet_1mm, os.path.join(path_out_1mm, 'PET.nii.gz'))
        sitk.WriteImage(t1_1mm, os.path.join(path_out_1mm, 'T1.nii.gz'))
        sitk.WriteImage(t2_1mm, os.path.join(path_out_1mm, 'T2.nii.gz'))
        sitk.WriteImage(gtvt_1mm, os.path.join(path_out_1mm, 'GTVt.nii.gz'))

        sitk.WriteImage(ct, os.path.join(path_out_ct, 'CT.nii.gz'))
        sitk.WriteImage(pet_3mm, os.path.join(path_out_ct, 'PET.nii.gz'))
        sitk.WriteImage(t1_3mm, os.path.join(path_out_ct, 'T1.nii.gz'))
        sitk.WriteImage(t2_3mm, os.path.join(path_out_ct, 'T2.nii.gz'))
        sitk.WriteImage(gtvt, os.path.join(path_out_ct, 'GTVt.nii.gz'))
    except:
        logger.info(f'!!!failed to write...{str(patient)}')
        return

    if not gtvn:
        gtv = gtvt
        gtv_1mm = reslice_image(gtv, ct_1mm, is_label = True)
    else:
        gtv = combine_gtvt_gtvn(gtvt, gtvn)
        gtv_1mm = reslice_image(gtv, ct_1mm, is_label = True)

    sitk.WriteImage(gtv_1mm, os.path.join(path_out_1mm, 'GTV.nii.gz'))
    sitk.WriteImage(gtv, os.path.join(path_out_ct, 'GTV.nii.gz'))



def resample_image(itk_image, out_spacing=[1.0, 1.0, 1.0], is_label=False):
    original_spacing = itk_image.GetSpacing()
    original_size = itk_image.GetSize()

    out_size = [
        int(np.round(original_size[0] * (original_spacing[0] / out_spacing[0]))),
        int(np.round(original_size[1] * (original_spacing[1] / out_spacing[1]))),
        int(np.round(original_size[2] * (original_spacing[2] / out_spacing[2])))
    ]

    resample = sitk.ResampleImageFilter()
    resample.SetOutputSpacing(out_spacing)
    resample.SetSize(out_size)
    resample.SetOutputDirection(itk_image.GetDirection())
    resample.SetOutputOrigin(itk_image.GetOrigin())
    resample.SetTransform(sitk.Transform())
    resample.SetDefaultPixelValue(itk_image.GetPixelIDValue())

    if is_label:
        resample.SetInterpolator(sitk.sitkNearestNeighbor)
    else:
        resample.SetInterpolator(sitk.sitkBSpline)

    return resample.Execute(itk_image)

def reslice_image(itk_image, itk_ref, is_label=False):
    resample = sitk.ResampleImageFilter()
    resample.SetReferenceImage(itk_ref)

    if is_label:
        resample.SetInterpolator(sitk.sitkNearestNeighbor)
    else:
        resample.SetInterpolator(sitk.sitkBSpline)

    return resample.Execute(itk_image)


@click.command()
@click.argument('input_folder', type=click.Path(exists=True), default=path_in)
@click.argument('path_out_1mm', type=click.Path(), default=path_out_1mm)
@click.argument('path_out_ct', type=click.Path(), default=path_out_ct)
@click.option('--cores',
              type=click.INT,
              default=64,
              help='The number of workers for parallelization.')
@click.option('--resampling',
              type=click.FLOAT,
              nargs=3,
              default=(1, 1, 1),
              help='Expect 3 positive floats describing the output '
              'resolution of the resampling. To avoid resampling '
              'on one or more dimension a value of -1 can be fed '
              'e.g. --resampling 1.0 1.0 -1 will resample the x '
              'and y axis at 1 mm/px and left the z axis untouched.')
@click.option('--order',
              type=click.INT,
              nargs=1,
              default=3,
              help='The order of the spline interpolation used to resample')
def main(input_folder, path_out_1mm, path_out_ct, cores, resampling,
         order):
    """ This command line interface allows to resample NIFTI files within a
        given bounding box contain in BOUNDING_BOXES_FILE. The images are
        resampled with spline interpolation
        of degree --order (default=3) and the segmentation are resampled
        by nearest neighbor interpolation.

        INPUT_FOLDER is the path of the folder containing the NIFTI to
        resample.
        OUTPUT_FOLDER is the path of the folder where to store the
        resampled NIFTI files.
        BOUNDING_BOXES_FILE is the path of the .csv file containing the
        bounding boxes of each patient.
    """
    logger = logging.getLogger(__name__)
    logger.info('Resampling started')
    if not os.path.exists(path_out_1mm):
        os.mkdir(path_out_1mm)
    if not os.path.exists(path_out_ct):
        os.mkdir(path_out_ct)
    print('resampling is {}'.format(str(resampling)))


    folder_list = sorted(glob.glob(os.path.join(input_folder, '*')))

    list_of_args = []
    for _, patient in enumerate(folder_list):
        list_of_args.append((patient, path_out_1mm, path_out_ct))

    with Pool(cores) as p:
        p.starmap(resampler_sitk, list_of_args)
        #p.starmap(resampler, list_of_args)

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logging.captureWarnings(True)

    main()
