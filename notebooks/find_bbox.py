import matplotlib.pyplot as plt

import numpy as np
import SimpleITK as sitk
from scipy.ndimage import gaussian_filter
from scipy.ndimage.measurements import label

import pdb
from os.path import join
import numpy as np

from scipy.ndimage import gaussian_filter
from scipy.ndimage.measurements import label
from scipy.ndimage.morphology import generate_binary_structure
from scipy.ndimage.measurements import center_of_mass

def bbox_auto_pet(sitk_pt, output_shape=(192, 192, 192), th=3):
    """Find a bounding box automatically based on the SUV

    Arguments:
        vol_pt {numpy array} -- The PET volume on which to compute the bounding box
        px_spacing_pt {tuple of float} -- The spatial resolution of the PET volume 
        px_origin_pt {tuple of float} -- The spatial position of the first voxel in the PET volume 

    Keyword Arguments:
        shape {tuple} -- The ouput size of the bounding box in millimeters
        th {float} -- [description] (default: {3})

    Returns:
        [type] -- [description]
    """
    np_pt = np.transpose(sitk.GetArrayFromImage(sitk_pt), (2, 1, 0))
    px_spacing_pt = sitk_pt.GetSpacing()
    px_origin_pt = sitk_pt.GetOrigin()

    output_shape_pt = tuple(e1 / e2
                            for e1, e2 in zip(output_shape, px_spacing_pt))
    # Gaussian smooth
    np_pt_gauss = gaussian_filter(np_pt, sigma=3)
    # auto_th: based on max SUV value in the top of the PET scan
    #auto_th = np.max(np_pt[:, :, np.int(np_pt.shape[2] * 2 // 3):]) / 4
    #print('auto_th = ', auto_th)
    # OR fixed threshold
    np_pt_thgauss = np.where(np_pt_gauss > th, 1, 0)
    # Find brain as biggest blob AND not in lowest third of the scan
    labeled_array, _ = label(np_pt_thgauss)
    print( "!!!shape is :",labeled_array.shape, np_pt_thgauss.shape)
    try:
        np_pt_brain = labeled_array == np.argmax(
            np.bincount(labeled_array[:, :,
                                      np_pt.shape[2] * 2 // 3:].flat)[1:]) + 1
    except:
        print('th too high?')
        # Quick fix just to pass for all cases
        th = 0.1
        np_pt_thgauss = np.where(np_pt_gauss > th, 1, 0)
        labeled_array, _ = label(np_pt_thgauss)
        np_pt_brain = labeled_array == np.argmax(
            np.bincount(labeled_array[:, :,
                                      np_pt.shape[2] * 2 // 3:].flat)[1:]) + 1
    # Find lowest voxel of the brain and box containing the brain
    z = np.min(np.argwhere(np.sum(np_pt_brain, axis=(0, 1))))
    y1 = np.min(np.argwhere(np.sum(np_pt_brain, axis=(0, 2))))
    y2 = np.max(np.argwhere(np.sum(np_pt_brain, axis=(0, 2))))
    x1 = np.min(np.argwhere(np.sum(np_pt_brain, axis=(1, 2))))
    x2 = np.max(np.argwhere(np.sum(np_pt_brain, axis=(1, 2))))

    # Center bb based on this brain segmentation
    zshift = 30 // px_spacing_pt[2]
    if z - (output_shape_pt[2] - zshift) < 0:
        zbb = (0, output_shape_pt[2])
    elif z + zshift > np_pt.shape[2]:
        zbb = (np_pt.shape[2] - output_shape_pt[2], np_pt.shape[2])
    else:
        zbb = (z - (output_shape_pt[2] - zshift), z + zshift)

    yshift = 30 // px_spacing_pt[1]
    if np.int((y2 + y1) / 2 - yshift - np.int(output_shape_pt[1] / 2)) < 0:
        ybb = (0, output_shape_pt[1])
    elif np.int((y2 + y1) / 2 - yshift -
                np.int(output_shape_pt[1] / 2)) > np_pt.shape[1]:
        ybb = np_pt.shape[1] - output_shape_pt[1], np_pt.shape[1]
    else:
        ybb = ((y2 + y1) / 2 - yshift - output_shape_pt[1] / 2,
               (y2 + y1) / 2 - yshift + output_shape_pt[1] / 2)

    if np.int((x2 + x1) / 2 - np.int(output_shape_pt[0] / 2)) < 0:
        xbb = (0, output_shape_pt[0])
    elif np.int((x2 + x1) / 2 -
                np.int(output_shape_pt[0] / 2)) > np_pt.shape[0]:
        xbb = np_pt.shape[0] - output_shape_pt[0], np_pt.shape[0]
    else:
        xbb = ((x2 + x1) / 2 - output_shape_pt[0] / 2,
               (x2 + x1) / 2 + output_shape_pt[0] / 2)

    z_pt = np.asarray(zbb)
    y_pt = np.asarray(ybb)
    x_pt = np.asarray(xbb)

    # In the physical dimensions
    z_abs = z_pt * px_spacing_pt[2] + px_origin_pt[2]
    y_abs = y_pt * px_spacing_pt[1] + px_origin_pt[1]
    x_abs = x_pt * px_spacing_pt[0] + px_origin_pt[0]

    bb = np.asarray((x_abs, y_abs, z_abs)).flatten()
    return bb


def bbox_auto(vol_pt, gtvt, px_spacing_pt, px_spacing_ct, px_origin_pt, px_origin_ct, output_shape=(128, 128, 128), th = 3, auto_th = False, bbox=None):
    # We find the oropharynx region from the PET based on brain segmentation
    output_shape_pt = tuple(e1 // e2 for e1, e2 in zip(output_shape, px_spacing_pt)) 
    print(f"output_shape_pt, {output_shape_pt}")
    # Gaussian smooth
    vol_pt_gauss = gaussian_filter(vol_pt, sigma=3)
    # auto_th: based on max SUV value in the top of the PET scan, for some cases that have unusual SUV values
    if auto_th: 
        th = np.max(vol_pt[np.int(vol_pt.shape[0] * 2 // 3):, :, :]) / 2.6
        print ('auto_th = ', th, '----------------------------------')
    # OR fixed threshold (for all other cases)
    vol_pt_thgauss = np.where(vol_pt_gauss > th, 1, 0)
    # Find brain as biggest blob AND not in lowest third of the scan
    labeled_array, _ = label(vol_pt_thgauss)
    print( "!!!shape is :",labeled_array.shape, vol_pt_thgauss.shape)
    plt.imshow(labeled_array[:, :, 120])
    plt.show()
    try:
        vol_pt_brain = labeled_array == np.argmax(np.bincount(labeled_array[vol_pt.shape[0] * 2 // 3:].flat)[1:]) + 1
    except: 
        print('th too high?')
        # Quick fix just to pass for all cases
        th = 0.1
        vol_pt_thgauss = np.where(vol_pt_gauss > th, 1, 0)
        labeled_array, _ = label(vol_pt_thgauss)
        vol_pt_brain = labeled_array == np.argmax(np.bincount(labeled_array[vol_pt.shape[0] * 2 // 3:].flat)[1:]) + 1
    # Find lowest voxel of the brain and box containing the brain
    z = np.min(np.argwhere(np.sum(vol_pt_brain, axis=(1, 2))))
    y1 = np.min(np.argwhere(np.sum(vol_pt_brain, axis=(0, 2))))
    y2 = np.max(np.argwhere(np.sum(vol_pt_brain, axis=(0, 2))))
    x1 = np.min(np.argwhere(np.sum(vol_pt_brain, axis=(0, 1))))
    x2 = np.max(np.argwhere(np.sum(vol_pt_brain, axis=(0, 1))))
    
    # Center bb based on this
    zshift = 30//px_spacing_pt[2]
    if z - (output_shape_pt[2] - zshift) < 0:
        zbb = (0, output_shape_pt[2])
    elif z + zshift > vol_pt.shape[0]:
        zbb = (vol_pt.shape[0] - output_shape_pt[2], vol_pt.shape[0])
    else:
        zbb = (z - (output_shape_pt[2] - zshift), z + zshift)

    yshift = 30//px_spacing_pt[1]
    if np.int((y2 + y1) / 2 - yshift - np.int(output_shape_pt[1] / 2)) < 0:
        ybb = (0, output_shape_pt[1])
    elif np.int((y2 + y1) / 2 - yshift - np.int(output_shape_pt[1] / 2)) > vol_pt.shape[1]:
        ybb = vol_pt.shape[1] - output_shape_pt[1], vol_pt.shape[1]
    else:
        ybb = (np.int((y2 + y1) / 2 - yshift - np.int(output_shape_pt[1] / 2)), np.int((y2 + y1) / 2 - yshift + np.int(output_shape_pt[1] / 2)))

    if np.int((x2 + x1) / 2 - np.int(output_shape_pt[0] / 2)) < 0:
        xbb = (0, output_shape_pt[0])
    elif np.int((x2 + x1) / 2 - np.int(output_shape_pt[0] / 2)) > vol_pt.shape[2]:
        xbb = vol_pt.shape[2] - output_shape_pt[0], vol_pt.shape[2]
    else:
        xbb = (np.int((x2 + x1) / 2 - np.int(output_shape_pt[0] / 2)), np.int((x2 + x1) / 2 + np.int(output_shape_pt[0] / 2)))

    print(zbb, ybb, xbb)
    z_pt = np.asarray(zbb,dtype=np.int)
    y_pt = np.asarray(ybb,dtype=np.int)
    x_pt = np.asarray(xbb,dtype=np.int)

    # In the physical dimensions
    z_abs = z_pt * px_spacing_pt[2] + px_origin_pt[2]
    y_abs = y_pt * px_spacing_pt[1] + px_origin_pt[1]
    x_abs = x_pt * px_spacing_pt[0] + px_origin_pt[0]
    
    # In the CT resolution:
    z_ct = np.asarray((z_abs-px_origin_ct[2])//px_spacing_ct[2],dtype=np.int)
    y_ct = np.asarray((y_abs-px_origin_ct[1])//px_spacing_ct[1],dtype=np.int)
    x_ct = np.asarray((x_abs-px_origin_ct[0])//px_spacing_ct[0],dtype=np.int)
    print(z_ct,y_ct,x_ct)

    # Check that the bbox contains the tumors
    fail = False
    if np.sum(gtvt[z_ct[0]:z_ct[1], y_ct[0]:y_ct[1], x_ct[0]:x_ct[1]]) != np.sum(gtvt):
        print('GTVt outside bbox ------------------------------------')
        fail = True
    # Add the fails for which we had to change the threshold to keep track
    if auto_th:
        fail = True
    
    
    if bbox is not None:
        x_abs = bbox[0:2]
        y_abs = bbox[2:4]
        z_abs = bbox[4:6]
        
        z_pt = np.asarray((z_abs - px_origin_pt[2])/ px_spacing_pt[2],dtype=np.int)
        y_pt = np.asarray((y_abs - px_origin_pt[1])/ px_spacing_pt[1],dtype=np.int)
        x_pt = np.asarray((x_abs - px_origin_pt[0])/ px_spacing_pt[0],dtype=np.int)
        
        z_ct = np.asarray((z_abs-px_origin_ct[2])//px_spacing_ct[2],dtype=np.int)
        y_ct = np.asarray((y_abs-px_origin_ct[1])//px_spacing_ct[1],dtype=np.int)
        x_ct = np.asarray((x_abs-px_origin_ct[0])//px_spacing_ct[0],dtype=np.int)
        #x_pt = np.asarray([50,76],dtype=np.int)
        #y_pt = np.asarray([43,70],dtype=np.int)
        #z_pt = np.asarray([212,256],dtype=np.int)
        #print (x_pt,y_pt,z_pt)
        #z_abs = z_pt * px_spacing_pt[2] + px_origin_pt[2]
        #y_abs = y_pt * px_spacing_pt[1] + px_origin_pt[1]
        #x_abs = x_pt * px_spacing_pt[0] + px_origin_pt[0]
        #pdb.set_trace()
        
        if np.sum(gtvt[z_ct[0]:z_ct[1], y_ct[0]:y_ct[1], x_ct[0]:x_ct[1]]) != np.sum(gtvt):
            print('still GTVt outside bbox ------------------------------------')
        else: 
            print('now GTVt inside bbox ------------------------------------')
    
    # Plot box on vol_pt_brain for visualization
    vol_pt_brain[z_pt[0]:z_pt[1], y_pt[0]:y_pt[0] + 1, x_pt[0]:x_pt[1]] = True
    vol_pt_brain[z_pt[0]:z_pt[1], y_pt[1]:y_pt[1] + 1, x_pt[0]:x_pt[1]] = True

    vol_pt_brain[z_pt[0]:z_pt[1], y_pt[0]:y_pt[1], x_pt[0]:x_pt[0] + 1] = True
    vol_pt_brain[z_pt[0]:z_pt[1], y_pt[0]:y_pt[1], x_pt[1]:x_pt[1] + 1] = True

    vol_pt_brain[z_pt[0]:z_pt[0] + 1, y_pt[0]:y_pt[1], x_pt[0]:x_pt[1]] = True
    vol_pt_brain[z_pt[1]:z_pt[1] + 1, y_pt[0]:y_pt[1], x_pt[0]:x_pt[1]] = True
    
        
    return vol_pt_brain, fail, z_abs, y_abs, x_abs


def write_nii(wrt, img, path):
    wrt.SetFileName(path)
    wrt.Execute(img)

def check_singleGTVt(gtvt):
    s = generate_binary_structure(2,2)
    labeled_array, num_features = label(gtvt)
    if num_features !=1:
        print('num_features-------------------------------',num_features)
        print('number of voxels:')
        for i in np.unique(labeled_array)[1:]:
            print (np.sum(labeled_array==i))
        print('centers:')
        for i in np.unique(labeled_array)[1:]:
            print (center_of_mass(labeled_array==i))
    return 0