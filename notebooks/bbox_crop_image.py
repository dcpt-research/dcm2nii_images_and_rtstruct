import SimpleITK as sitk


import os
from os.path import join
import numpy as np
import pandas as pd
import nibabel as nib

import csv
from scipy.ndimage import gaussian_filter
from scipy.ndimage.measurements import label
from scipy.ndimage.morphology import generate_binary_structure
from scipy.ndimage.measurements import center_of_mass
# %matplotlib nbagg

from find_bbox import *

if __name__ == "__main__":
    num_workers = 8
    output_shape = (192,192,192)
    input_path = 'C://data//nifti//'
    output_path = 'C://data//cropped//'

    try:
        os.mkdir(output_path)
        print("Directory ", output_path, " Created ")
    except FileExistsError:
        print("Directory ", output_path, " already exists")

    writer = sitk.ImageFileWriter()
    writer.SetImageIO("NiftiImageIO")

    with open(join(output_path,'../bbipynb.csv'), 'w', newline='') as csvfile:
        bbwrite = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        bbwrite.writerow(['PatientID', 'x1', 'x2', 'y1', 'y2', 'z1', 'z2'])

    patients = []
    for f in sorted(os.listdir(input_path)):
        patients.append(f)
    nfail = 0
    n=0
    list_auto_th = ['CHUM010','CHUS021','CHGJ026','CHMR023','CHGJ053','CHMR028']
    list_fix_bb = ['CHMR028','CHGJ053','CHGJ082']
    dict_fix_bb = {
        "CHMR028": np.asarray([-73.828125,68.359375,-112.109375,35.546875,-204.0536231994629,-60.17230224609375
    ]),
        "CHGJ053": np.asarray([-86.1328125,54.4921875,-166.9921875,-26.3671875,-214.2802734375,-70.4007568359375]),
        "CHGJ082": np.asarray([-68.5546875,72.0703125,-170.5078125,-29.8828125,-245.0201416015625,-101.140625])
            }

    for patient in patients[:1]:
        #list_p = ['HN-CHUM-020','HN-CHUM-026','HN-CHUM-030','HN-CHUM-042','HN-CHUM-053','HN-CHUM-057','HN-CHUM-065','HN-CHUS-010','HN-CHUS-035','HN-CHUS-045','HN-CHUS-057','HN-CHUS-074','HN-CHUS-086','HN-CHUS-096','HN-HGJ-025','HN-HGJ-062','HN-CHUM-053','HN-CHUM-053','HN-CHUM-053','HN-CHUM-053','HN-CHUM-053','HN-CHUM-053','HN-CHUM-053','HN-CHUM-053','HN-CHUM-053']
        # # pdb.set_trace()
        #if patient not in list_auto_th[:4]:
        #    continue
        #if patient not in ['HN-HMR-028','HN-HGJ-053','HN-HGJ-082']:
        #    continue
        print('************* patient:', patient)
#         in_path_ct = os.path.join(input_path,patient, 'CT_3mm.nii.gz')
#         in_path_pt = os.path.join(input_path,patient, 'PET.nii.gz')
#         in_path_gtvt_roi = os.path.join(input_path,patient, 'DIL_GTV.nii.gz')
        in_path_ct = os.path.join(input_path,patient, 'ct.nii.gz')
        in_path_pt = os.path.join(input_path,patient, 'pt.nii.gz')
        in_path_gtvt_roi = os.path.join(input_path,patient, 'gtv_t.nii.gz')
        if not os.path.exists(in_path_gtvt_roi):
            print('no GTVt')
        #in_path_gtvn_roi = input_paths + patient + '/' + patient + '_ct_gtvn.nii.gz'

        #out_path_bbox = output_path + patient + '/' + patient + '_ct_bbox'
        try: 
            img_ct = sitk.ReadImage(in_path_ct)
            #img_ct.SetDirection(tuple(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0))
            img_pt = sitk.ReadImage(in_path_pt)
            #img_ct.SetDirection(tuple(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0))
        except:
            print('cannot read ------------')
            continue
        px_spacing_ct = img_ct.GetSpacing()
        px_spacing_pt = img_pt.GetSpacing()
        px_origin_ct = img_ct.GetOrigin()
        px_origin_pt = img_pt.GetOrigin()

        img_ct = sitk.GetArrayFromImage(img_ct)
        orentiation = img_pt.GetDirection()
        #set to RAI
        #image_out
        gtvt_img = sitk.ReadImage(in_path_gtvt_roi)
        gtvt = sitk.GetArrayFromImage(gtvt_img)
        check_singleGTVt(gtvt)
        #gtvn = sitk.GetArrayFromImage(sitk.ReadImage(in_path_gtvn_roi))
        img_pt = sitk.GetArrayFromImage(sitk.ReadImage(in_path_pt))
        
        # Fix threshold for some of the patients:
        auto_th = True
        if patient in list_auto_th[:4]:
            auto_th = True
        # Fix directly the bbox for some that don't work
        bbox = None
        if patient in list_fix_bb:
            bbox = dict_fix_bb[patient]

        #Direction RAI
        img_brain, fail, z_bb, y_bb, x_bb = bbox_auto(img_pt, gtvt, px_spacing_pt, px_spacing_ct, px_origin_pt, px_origin_ct, output_shape, auto_th=auto_th, bbox=bbox)
        nfail = nfail + fail
        n = n + 1
        perm = (0, 1, 2)  # No permutation needed now
        img_brain = sitk.GetImageFromArray(np.transpose(img_brain.astype(np.uint8), perm), isVector=False)
        # img_pt = sitk.GetImageFromArray(np.transpose(img_pt, perm), isVector=False)
        out_path_brain = output_path + patient + '_brain.nii'
        write_nii(writer, img_brain, out_path_brain)
        
        # Write bb position in csv. To change to panda frame
        with open(join(output_path,'../bbipynb.csv'), 'a', newline='') as csvfile:
            bbwrite = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            bbwrite.writerow([patient, str(x_bb[0]), str(x_bb[1]), str(y_bb[0]), str(y_bb[1]), str(z_bb[0]), str(z_bb[1])])

    print ('fails/total',nfail,n)
